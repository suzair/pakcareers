<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends CI_Controller {

	public $data=array();
	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->userdata[0]['username']))
		{
			redirect('admin/index');
			
		}				
	}

	public function recentnews($mode='view',$id=0)
	{
		$rules=$this->recentnews_model->validationrules;
		
		$this->form_validation->set_rules($rules);
		
		$data['admin']=$this->session->userdata[0];
		$data['view']="recentnews";
		$data['title']=" Welcome to Pak Jobs | Add News";		
		
		//print_r($country->result_array());
		
		$data['recentnews']=$this->recentnews_model->fetchAll();
		switch($mode)
		{
			case 'view':	
					$data['subtitle']="Add News";				
					$data['title']=" Welcome to Pak Careers | Ad dNews";					
					//$data['method']='add';

					$this->load->view('admin/dashboard',$data);
				break;
			case 'add':
					
					$insertarray=$this->input->post();
					//print_r($insertarray); die;
					
					if(count($_POST)>0){
						
						if ($this->form_validation->run() === true) {
							
							if(!$id){
								$news_id=$this->general_model->save("recent_news",$insertarray,true);
							}else{
									
									$this->general_model->update("recent_news",$insertarray,"news_id=".$id);
									$news_id=$id;
							}
							//echo $id;
							if($news_id){							
										
										$data['view']="recentnews";
										$data['subtitle']="Add News";	
										$data['title']=" Welcome to Pak Careers | Add News";
										$data['messrecentnews']=" Record Entered Successfully";
										$data['recentnewss']=$this->recentnews_model->fetchAll();		
										$this->load->view('admin/dashboard', $data);
										redirect("content/recentnews");
									
									
							}
						}else{
						
								$data['view']="recentnews";
								$data['subtitle']="Add News";	
								$data['title']=" Welcome to Pak Careers | Add News";								
								$data['recentnewss']=$this->recentnews_model->fetchAll();		
								$this->load->view('admin/dashboard', $data);
						}
					}else{

							redirect("content/recentnews");
					}
					
				break;
			 case 'delete':
			 		if($this->recentnews_model->deActiveNews($id))
					{
						redirect("content/recentnews");
					}
			 	break;
			case 'active':
			 		if($this->recentnews_model->Activerecentnews($id))
					{
						redirect("content/recentnews");
					}
			 	break;
			case 'edit':
				if($id){
					$data['subtitle']="Update News Data" ;				
					$data['title']=" Welcome to Pak Jobs | Update News";					
					
					$recentnews = $this->recentnews_model->getrecentnewsById($id);
					//print_r($recentnews);
					foreach ($recentnews as $key => $field)
					{
						$data[$key]=$field;	
					}
					
					$this->load->view('admin/dashboard',$data);
				}
				break;
			
		}
	}


	public function pages($mode='view',$id=0)
	{
		$rules=$this->pages_model->validationrules;
		
		$this->form_validation->set_rules($rules);
		
		$data['admin']=$this->session->userdata[0];
		$data['view']="pages";
		$data['title']=" Welcome to Pak Jobs | Add Page";		
		
		//print_r($country->result_array());
		
		$data['pages']=$this->pages_model->fetchAll();
		switch($mode)
		{
			case 'view':	
					$data['subtitle']="Add Page";				
					$data['title']=" Welcome to Pak Careers | Add Pages";					
					//$data['method']='add';


					$this->load->view('admin/dashboard',$data);
				break;
			case 'add':
					
					$insertarray=$this->input->post();
					
					if(count($_POST)>0){
						
						if ($this->form_validation->run() === true) {
							
							if(!$id){
								$page_id=$this->general_model->save("pages",$insertarray,true);
							}else{
									
									$this->general_model->update("pages",$insertarray,"page_id=".$id);
									$page_id=$id;
							}
							//echo $id;
							if($page_id){							
										
										$data['view']="pages";
										$data['subtitle']="Add Page";	
										$data['title']=" Welcome to Pak Careers | Add Page";
										$data['messpages']=" Record Entered Successfully";
										$data['pages']=$this->pages_model->fetchAll();		
										$this->load->view('admin/dashboard', $data);
										redirect("content/pages");
									
									
							}
						}else{
						
								$data['view']="pages";
								$data['subtitle']="Add Page";	
								$data['title']=" Welcome to Pak Careers | Add Page";								
								$data['pages']=$this->pages_model->fetchAll();		
								$this->load->view('admin/dashboard', $data);
						}
					}else{

							redirect("content/pages");
					}
					
				break;
			 case 'delete':
			 		if($this->pages_model->deActivePage($id))
					{
						redirect("content/pages");
					}
			 	break;
			case 'active':
			 		if($this->pages_model->ActivePages($id))
					{
						redirect("content/pages");
					}
			 	break;
			case 'edit':
				if($id){
					$data['subtitle']="Update Pages Data" ;				
					$data['title']=" Welcome to Pak Jobs | Update Pages";					
					
					$pages = $this->pages_model->getpagesById($id);
					//print_r($pages);
					foreach ($pages as $key => $field)
					{
						$data[$key]=$field;	
					}
					
					$this->load->view('admin/dashboard',$data);
				}
				break;
			
		}
	}
	
	

	
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */