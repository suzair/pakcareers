<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {

	public $settings ;
	public function __construct(){
		parent::__construct();
		$this->settings =  $this->settings_model->getsettings();
	}

	

	public function index()
	{	

		$data['settings'] = $this->settings;
		$data['view'] = "front_panel";
 		$data['latest_news'] = $this->recentnews_model->fetchActive();
 		$data['countries'] = $this->country_model->get_country_drop_down();
 		$data['cities'] = $this->city_model->get_city_drop_down();
		$data['job_sectors'] = $this->jobsector_model->get_job_sector_drop_down();
		$data['job_groups'] = $this->jobgroup_model->get_job_group_drop_down();
		$data['field_of_works'] = $this->fieldofwork_model->get_field_of_work_drop_down();
		$data['latest_jobs'] = $this->job_model->fetchActive("0,10");
		$data['level_of_educations'] =  $this->course_model->get_level_of_education_drop_down();
		$this->load->view('index',$data);
	}

	public function advance_jobs(){
		$data['settings'] = $this->settings;
		$data['job_calendar_results'] = $this->job_model->get_jobs_by_job_source();
		$data['salary_ranges'] = $this->salaryrange_model->get_salary_range_drop_down();
		$data['job_sources'] = $this->jobsource_model->fetchActive();
		$data['job_sectors'] = $this->jobsector_model->get_job_sector_drop_down();
		$data['field_of_works'] = $this->fieldofwork_model->get_field_of_work_drop_down();
		$data['educations'] = $this->education_model->get_education_drop_down();
		$data['organizations'] = $this->organization_model->get_oraganization_drop_down();
		$data['occupations'] = $this->organization_model->get_oraganization_drop_down();
		$data['cities'] = $this->city_model->get_city_drop_down();
  
		$data['view'] = "advance_job";
		$this->load->view("index", $data);
	}

	public function advance_course(){
		$data['settings'] = $this->settings;
		$data['degree_programs'] = $this->course_model->get_degree_drop_down();
		$data['field_of_educations'] = $this->course_model->get_field_of_education_drop_down();
		$data['level_of_educations'] =  $this->course_model->get_level_of_education_drop_down();

		$data['cities'] = $this->city_model->get_city_drop_down();
  
		$data['view'] = "advance_course";
		$this->load->view("index", $data);
	}

	public function advance_career(){
		$data['settings'] = $this->settings;
		$data['job_calendar_results'] = $this->job_model->get_jobs_by_job_source();
		$data['salary_ranges'] = $this->salaryrange_model->get_salary_range_drop_down();
		$data['job_sources'] = $this->jobsource_model->fetchActive();
		$data['job_sectors'] = $this->jobsector_model->get_job_sector_drop_down();
		$data['field_of_works'] = $this->fieldofwork_model->get_field_of_work_drop_down();
		$data['educations'] = $this->education_model->get_education_drop_down();
		$data['organizations'] = $this->organization_model->get_oraganization_drop_down();
		$data['occupations'] = $this->organization_model->get_oraganization_drop_down();
		$data['cities'] = $this->city_model->get_city_drop_down();
  
		$data['view'] = "advance_career";
		$this->load->view("index", $data);
	}

	public function job_details(){
		
	}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */