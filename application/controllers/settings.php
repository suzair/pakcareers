<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {

	public $data=array();
	public function __construct()
	{
		parent::__construct();
		if(!isset($this->session->userdata[0]['username']))
		{
			redirect('admin/index');
			
		}
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '4000';
		$config['max_width']  = '4000';
		$config['max_height']  = '2000';
		$this->load->library('upload', $config);		
	}
	

	/**
	* manage age group section
	**/
	public function general($mode='view',$id=0)
	{
		$setting_id = '';
		
		//echo phpinfo(); die();
		$rules=$this->settings_model->validationrules;
		$this->form_validation->set_rules($rules);
		
		$data['admin']=$this->session->userdata[0];
		$data['view']="general";
		$data['title']=" Welcome to PakCareer | Configure Settings";		
		
		switch($mode)
		{
			 case 'view':
				
					$settings  = $this->settings_model->getsettings();	
					//print_r($settings);
					//$settings_result = $settings[0];
					
					if(count($settings) > 0 ){ 
						foreach ($settings as $key=>$field)
						{
							$data[$key]=$field;	
							//echo $key;
						}
					}
					//echo '<pre>';
					//print_r($this->data);

					$data['subtitle']="Manage Settings";				
					$data['title']=" Welcome to PakCareer | Configure Settings";					
					
					$this->load->view('admin/dashboard',$data);
				break;
			case 'add':
					
					$insertarray=$this->input->post();
					//print_r($insertarray);
				
					if(count($_POST)>0){
						
						foreach($insertarray as $key=>$val)
						{
							$this->data[$key]=$val;
						}
						if ($this->form_validation->run() === true) {
							
							$settings  = $this->settings_model->getsettings();	
					
							if(count($settings) > 0 ){ 
								
									$this->general_model->update("settings",$insertarray, $settings->settings_id);
									$setting_id = $settings->settings_id;
							}else{
								
									$setting_id=$this->general_model->save("settings",$insertarray,true);
							}
 							
							if($setting_id){

				
								if ( ! $this->upload->do_upload('logo') && isset($_POST['logo']) )
									{
				
										$data['error'] =$this->upload->display_errors();					
												
										//$this->load->view('admin/dashboard', $this->data);
									}
									else
									{
				
										$imagedata = $this->upload->data();
										if(count($imagedata)>0){
											$updatearray=array("logo"=>$imagedata['file_name']);
											$this->general_model->update("settings",$updatearray,"settings_id=".$setting_id);
										}
										//print_r($data['upload_data']); die;											
										$data['message']=" Record Entered Successfully";		
										//$this->load->view('admin/dashboard', $this->data);
										redirect("settings/general");
									}


										
									$data['view']="general";
									$data['subtitle']=" Modify Settings";	
									$data['title']=" Welcome to PakCareer | Modify Settings";
									$data['message']=" Record Entered Successfully";
									
									$this->load->view('admin/dashboard', $data);
									$this->session->set_userdata("message", "Entered Successfully");
									//redirect("settings/general");
									
									
							}
						}else{
							
								$data['view']="general";
								$data['subtitle']="Modify Settings";	
								$data['title']=" Welcome to PakCareer | Modify Settings";								
								
								$this->load->view('admin/dashboard', $data);
								redirect("settings/general");
						}
					}else{
							redirect("settings/general");
					}
					// upload image
						
					// update entry with image
					
					//$data['subtitle']="Add Age Group";				
					//$data['title']=" Welcome to PakCareer | Add Age Group";					
					//$this->load->view('admin/dashboard',$data);
				break;
			 
		
			
			
		}
	}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */