<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public $data ;
	function __construct(){
		parent:: __construct();
		if(!isset($this->session->userdata[0]['username']))
		{
			redirect('admin/index');
			
		}
	}


	function add_user($msg = ''){
		$this->data['admin']=$this->session->userdata[0];
		$this->data['users'] = $this->users_model->fetchAll();
		$this->data['title']=" Welcome to PakCareer | Add Users";
		$this->data['message']= $msg;
		$this->data['subtitle']=" Add Users";
		$this->data['view']="add_user";
		
		$this->load->view('admin/dashboard',$this->data);
	}
	
	function add($id){
		$insert_array =  $this->input->post();
		$insert_array['password'] = md5($insert_array['password']);
		if(count($insert_array) > 0){
			
			if(!$id){
				$user_id = $this->general_model->save("management",$insert_array,true);
			}else{
				$this->general_model->update("management",$insert_array,"user_id=".$id);
				$user_id = $id;
			}
			if($user_id){
				redirect("users/add_user/Update Successfully");
			}
		}
	}
	
	function edit($id){
			if($id){
					$this->data['subtitle']="Update User  Data" ;				
					$this->data['title']=" Welcome to Pak career | Update Users";					
					$this->data['view']="add_user";
					$users=$this->users_model->getusersById($id);
					$userfields=$this->db->get("management");
					foreach ($userfields->list_fields() as $field)
					{
						$this->data[$field]=$users->$field;	
					}
					$this->data['admin']=$this->session->userdata[0];
					$this->data['users'] = $this->users_model->fetchAll();
					$this->load->view('admin/dashboard',$this->data);
				}
				
	}
	
	function delete($id){
		$this->general_model->delete("management",array("user_id" => $id));
		$this->session->set_userdata("message","Deleted Successfully");
		redirect("users/add_user/Deleted Successfully");
	}
	
}
?>