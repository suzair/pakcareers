<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('short_date'))
{
	function short_date($date)
	{
		return date("d-m-Y", strtotime($date));

		
	}
}

if ( ! function_exists('date_with_month_and_days'))
{
	function date_with_month_and_days($date)
	{
		return date("M d", strtotime($date));

		
	}
}
?>