<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('random_element'))
{
	function random_element($array)
	{
		if ( ! is_array($array))
		{
			return $array;
		}

		return $array[array_rand($array)];
	}
}
?>