<?php
class course_model extends CI_Model{
	
	public $validationrules;
	
	function __construct()
	{
			parent::__construct();
			$this->validationrules=array(
				array(
                     'field'   => 'course_name', 
                     'label'   => 'Course Title', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				  array(
                     'field'   => 'course_levelofeducation', 
                     'label'   => 'Level of Education', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				  array(
                     'field'   => 'course_degreeprogram', 
                     'label'   => 'Degree Program', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				  array(
                     'field'   => 'course_duration', 
                     'label'   => 'Course Duration', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				  array(
                     'field'   => 'course_institution', 
                     'label'   => 'Institution', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				  array(
                     'field'   => 'city_id', 
                     'label'   => 'City', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				   array(
                     'field'   => 'course_fieldofeducation', 
                     'label'   => 'Field of Education', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
              
             
			);
	}
	public function fetchActive()
	{
		
		$data=$this->general_model->selectRecord("*","course as j
														LEFT JOIN studytrack as st 
														 		     ON st.studytrack_id=j.studytrack_id
														 LEFT JOIN city as c
														 			ON j.city_id=c.city_id",
														"course_status=1","course_name","","");
		//echo '<pre>'; print_r($data->result_array()); die;
		return $data;
		
	}
	public function fetchInActive()
	{
		
		$data=$this->general_model->selectRecord("*","course as j
														 LEFT JOIN studytrack as st 
														 		     ON st.studytrack_id=j.studytrack_id
														 LEFT JOIN city as c
														 			ON j.city_id=c.city_id
														","course_status=0","course_name","","");
		return $data;
		
	}
	public function fetchAll($offset='',$pages="")
	{
		//echo $pages;
		if($pages===""){
			//echo "FdfD";
			$limit="";
		}
		else{
		//	echo "33";
			$limit=$pages*$offset.",".$offset;
		}
			
		//echo "limit:".$limit."<br/>";
		$data=$this->general_model->selectRecord("*","course as j
														LEFT JOIN studytrack as st 
														 		     ON st.studytrack_id=j.studytrack_id
														 LEFT JOIN city as c
														 			ON j.city_id=c.city_id
														","","course_name",$limit,"");
			
		return $data;		
		
	}
	
	public function deActivecourse($id)
	{		
		if($this->general_model->deactiveteEntry("course","course_id=".$id))return true; 		
	}
	public function Activecourse($id)
	{
		
		if($this->general_model->activeteEntry("course","course_id=".$id))	return true;
		
		
	}
	
	public function getcourseById($id)
	{
		$course=$this->general_model->selectRecord("*","course","course_id=?","","",array($id));
		return $course->row();
		
	}

	public function get_level_of_education_drop_down(){
		$courses = $this->fetchActive();
		$arr['']="Select Level of Education";
		foreach($courses->result_array() as $row) {
			if(!empty($row['course_levelofeducation']))
				$arr[$row['course_levelofeducation']]=$row['course_levelofeducation'];
		}
		
		return $arr;
	}

	public function get_degree_drop_down(){
		$courses = $this->fetchActive();
		$arr['']="Select Degree / Training";
		foreach($courses->result_array() as $row) {
			if(!empty($row['course_degreeprogram']))
				$arr[$row['course_degreeprogram']]=$row['course_degreeprogram'];
		}
		
		return $arr;
	}

	public function get_field_of_education_drop_down(){
		$courses = $this->fetchActive();
		$arr['']="Select Field of Education ";
		foreach($courses->result_array() as $row) {
			if(!empty($row['course_fieldofeducation']))
				$arr[$row['course_fieldofeducation']]=$row['course_fieldofeducation'];
		}
		
		return $arr;
	}

	public function fetch_results($params){
		//print_r($params);
		$base_query = "SELECT * FROM course as co ";

		// JOB SECTOR APPEND
		$base_query .= " INNER JOIN city as c  ON c.city_id = co.city_id ";
		$base_query .= " INNER JOIN studytrack as st  ON st.studytrack_id = co.studytrack_id ";
		

		$where = " WHERE course_status = 1 ";
		if (isset($params['search_job_key_word']) &&!empty($params['search_job_key_word'])){
			
			$where.= " AND j.course_name  LIKE '%".$params['search_job_key_word']."%'";
		}

		

		if (!empty($params['city_id']) ){

			
			$where.= " AND co.city_id =".$params['city_id'];
		}

		if (!empty($params['level_of_education']) ){

			
			$where.= " AND co.course_levelofeducation ='".$params['level_of_education']."'";
		}

		if (!empty($params['field_of_education']) ){

			
			$where.= " AND co.course_fieldofeducation ='".$params['field_of_education']."'";
		}

		if (!empty($params['course_degrees']) ){

			
			$where.= " AND co.course_degreeprogram ='".$params['course_degrees']."'";
		}



		$results = $this->db->query($base_query.$where);
		return $results->result_array();
	}
}
?>