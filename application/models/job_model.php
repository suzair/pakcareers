<?php
class job_model extends CI_Model{
	
	public $validationrules;
	
	function __construct()
	{
			parent::__construct();
			$this->validationrules=array(
				array(
                     'field'   => 'job_title', 
                     'label'   => 'Job Title', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				  array(
                     'field'   => 'jobsector_id', 
                     'label'   => 'Jobsector', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				  /*array(
                     'field'   => 'city_id', 
                     'label'   => 'City', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				  array(
                     'field'   => 'country_id', 
                     'label'   => 'Country Name', 
                     'rules'   => 'trim|required|xss_clean'
                  ),*/
				  array(
                     'field'   => 'posted_date', 
                     'label'   => 'Job Posted Date', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				  array(
                     'field'   => 'organization_id', 
                     'label'   => 'Organization Name', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				  array(
                     'field'   => 'jobsource_id', 
                     'label'   => 'Job Source', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				 /* array(
                     'field'   => 'job_image', 
                     'label'   => 'Job Image', 
                     'rules'   => "file_required|file_min_size[10KB]|file_max_size[500KB]|file_allowed_type[image]|file_image_mindim[50,50]|file_image_maxdim[400,300]"
                  ),*/
				 	
              
             
			);
			$this->validationrulesfile=array(
				array(
                     'field'   => 'fieldofwork_id', 
                     'label'   => 'Field of Work', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
			);
			$this->validationrulesjobtrack=array(
				array(
                     'field'   => 'careertrack_id', 
                     'label'   => 'Career Track', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
			);
	}
	public function fetchActive($limit ="")
	{
		
		$data=$this->general_model->selectRecord("*","jobs as j
														 LEFT JOIN jobsector as js 
														 		     ON js.jobsector_id=j.jobsector_id
														 LEFT JOIN city as c
														 			ON j.city_id=c.city_id
														LEFT JOIN jobsource 
														 			ON j.jobsource_id=jobsource.jobsource_id",
														"jobs_status=1","job_title",$limit,"");
		//echo '<pre>'; print_r($data->result_array()); die;
		return $data;
		
	}
	public function fetchInActive()
	{
		
		$data=$this->general_model->selectRecord("*","jobs as j
														 LEFT JOIN jobsector as js 
														 		     ON js.jobsector_id=j.jobsector_id
														 LEFT JOIN city as c
														 			ON j.city_id=c.city_id
														LEFT JOIN jobsource 
														 			ON j.jobsource_id=jobsource.jobsource_id
														","jobs_status=0","job_title","","");
		return $data;
		
	}
	public function fetchAll($offset='',$pages="")
	{
		//echo "page".$pages;
		if($pages===""){
			//echo "FdfD";
			$limit="";
		}
		else{
		//	echo "33";
			$limit=$pages*$offset.",".$offset;
		}
			
		//echo "limit:".$limit."<br/>";
		$data=$this->general_model->selectRecord("*","jobs as j
														 LEFT JOIN jobsector as js 
														 		     ON js.jobsector_id=j.jobsector_id
														 LEFT JOIN city as c
														 			ON j.city_id=c.city_id
														LEFT JOIN jobsource 
														 			ON j.jobsource_id=jobsource.jobsource_id
														","","job_title",$limit,"");
			
		return $data;		
		
	}
	
	public function deActiveJobs($id)
	{		
		if($this->general_model->deactiveteEntry("jobs","job_id=".$id))return true; 		
	}
	public function ActiveJobs($id)
	{
		
		if($this->general_model->activeteEntry("jobs","job_id=".$id))	return true;
		
		
	}

	public function get_job_qualificaitons($job_id){
		$query =  "select * FROM education INNER JOIN jobs_qualification as jq ON jq.education_id = education.education_id 
					WHERE jq.job_id =".$job_id;
		$result = $this->db->query($query);
		return $result->result_array();
	}
	
	public function get_job_citites($job_id){
		$query =  "select * FROM city INNER JOIN jobs_cities as jc ON jc.city_id = city.city_id 
					WHERE jc.job_id =".$job_id;
		$result = $this->db->query($query);
		return $result->result_array();
	}

	public function getJobById($id)
	{

		$query =  " SELECT * FROM jobs as j
								LEFT JOIN country ON j.country_id = country.country_id
								LEFT JOIN jobgroup as jg ON jg.jobgroup_id = j.jobgroup_id
								LEFT JOIN age as a ON a.age_id = j.agegroup_id
								LEFT JOIN education as e ON e.education_id = j.education_id
								LEFT JOIN experience as ex ON ex.experience_id = j.experience_id
								LEFT JOIN salaryrange ON salaryrange.salaryrange_id = j.salaryrange_id
								LEFT JOIN organization ON organization.organization_id = j.organization_id
					WHERE j.job_id =".$id;
		//$job=$this->general_model->selectRecord("*","jobs","job_id=?","","",array($id));
		$job = $this->db->query($query);
		//print_r($job->result_array());
		return $job->row();
		
	}
	public function deleteFOW($id)
	{
		//echo $id;
		$this->db->delete("jobs_fieldofwork",array("jf_id"=>$id));
	}
	public function deleteCT($id)
	{
		//echo $id;
		$this->db->delete("jobs_careertrack",array("jc_id"=>$id));
	}
	
	public function deleteC($id)
	{
		//echo $id;
		$this->db->delete("jobs_cities",array("jobs_cities_id"=>$id));
	}
	public function deleteE($id)
	{
		//echo $id;
		$this->db->delete("jobs_qualification",array("job_qualification_id"=>$id));
	}
	public function fetchCareerFieldOfWork($job_id)
	{
		
		$data=$this->general_model->selectRecord("o.*,co.jf_id","jobs_fieldofwork as  co
														LEFT JOIN fieldofwork as o 
														 		     ON o.fieldofwork_id=co.fieldofwork_id
														 LEFT JOIN jobs as c
														 			ON co.job_id=c.job_id
														","","fieldofwork_title","","");
		return $data;
		
	}
	public function fetchCareerTracks($job_id)
	{
		
		$data=$this->general_model->selectRecord("o.*,co.jc_id","jobs_careertrack as  co
														LEFT JOIN careertrack as o 
														 		     ON o.careertrack_id=co.careertrack_id
														 LEFT JOIN jobs as c
														 			ON co.job_id=c.job_id
														","","careertrack_title","","");
		return $data;
		
	}
	
	public function fetchCities($job_id)
	{
		
		$data=$this->general_model->selectRecord("o.*,co.jobs_cities_id","jobs_cities as  co
														LEFT JOIN city as o 
														 		     ON o.city_id=co.city_id
														 LEFT JOIN jobs as c
														 			ON co.job_id=c.job_id
														","","city_name","","");
		return $data;
		
	}
	
	public function fetchEducations($job_id)
	{
		
		$data=$this->general_model->selectRecord("o.*,co.job_qualification_id","jobs_qualification as  co
														LEFT JOIN education as o 
														 		     ON o.education_id=co.education_id
														 LEFT JOIN jobs as c
														 			ON co.job_id=c.job_id
														","","education_title","","");
		return $data;
		
	}
	
	public function get_donut_by_gender()
	{
		$result = array();
		$data=$this->db->query("select distinct(gender) as gender, count(*) as jobs  from jobs group by gender");
		$dataTotal = $this->db->query("select count(*) as jobs  from jobs");
		$total = $dataTotal->row();
		$totalJobs = $total->jobs;
		//print_r($data->result_array());
		foreach($data->result_array() as $key=>$val){
			$gender = '"'.$val['gender'].'"';
			$count = intval(($val['jobs']/$totalJobs)*100);		
			$result[] = "[$gender,$count]";
		}
		return $result;
	}
	
	public function get_donut_by_job_source()
	{
		$result = array();
		$data=$this->db->query("select distinct(js.jobsource_title) as title, count(*) as jobs  from jobs as j INNER JOIN jobsource as js ON j.jobsource_id = js.jobsource_id  group by js.jobsource_title");
		$dataTotal = $this->db->query("select count(*) as jobs  from jobs");
		$total = $dataTotal->row();
		$totalJobs = $total->jobs;
		//print_r($data->result_array());
		foreach($data->result_array() as $key=>$val){
			$jobsource_title = '"'.$val['title'].'"';
			$count = intval(($val['jobs']/$totalJobs)*100);		
			$result[] = "[$jobsource_title,$count]";
		}
		return $result;
	}
	public function get_donut_by_job_city()
	{
		$result = array();
		$data=$this->db->query("select distinct(c.city_name) as title, count(*) as jobs  from jobs as j INNER JOIN city as c ON j.city_id = c.city_id  group by c.city_name");
		$dataTotal = $this->db->query("select count(*) as jobs  from jobs");
		$total = $dataTotal->row();
		$totalJobs = $total->jobs;
		//print_r($data->result_array());
		foreach($data->result_array() as $key=>$val){
			$jobsource_title = '"'.$val['title'].'"';
			$count = intval(($val['jobs']/$totalJobs)*100);		
			$result[] = "[$jobsource_title,$count]";
		}
		return $result;
	}

	public function get_jobs_by_job_source($job_source = ''){
		$results  =array();
		$job_source_total_job_count_query = "select count(*) as total_jobs, js.jobsource_title 
																		FROM jobs INNER JOIN jobsource as js ON
		
																												js.jobsource_id = jobs.jobsource_id
																		WHERE js.jobsource_status =1 and jobs.jobs_status = 1 ";
		
		$job_source_total_job_query = "select jobs.*, js.jobsource_title 
																		FROM jobs INNER JOIN jobsource as js ON
		
																												js.jobsource_id = jobs.jobsource_id";

		// condition 
		$query_additional_conditions = !empty($job_source) ? " AND  js.jobsource_title =".$job_source :""; 																										
		$job_source_total_job_query .= $query_additional_conditions;
		$job_source_total_job_count_query .= $query_additional_conditions;

		//query additional options 
		$query_group_options = " GROUP BY js.jobsource_title"; 
		$query_order_operation = " ORDER BY posted_date desc ";																											
		$job_source_total_job_query .= $query_order_operation ;
		$job_source_total_job_count_query .= $query_group_options. $query_order_operation ;
		
		$jobs_source_total_job_count_results =  $this->db->query($job_source_total_job_count_query);
		$job_source_count = array();
		foreach ($jobs_source_total_job_count_results->result_array() as $job){

			$job_source_count[$job['jobsource_title']] = $job['total_jobs'];
		}

		// get jobs details
		
		//echo $job_source_total_job_query;
		$jobs_source_total_job_results =  $this->db->query($job_source_total_job_query);
		$job_dates_array= array();
		foreach ($jobs_source_total_job_results->result_array() as $job){

			$job_source_results[$job['jobsource_title']][date("d-m-Y", strtotime($job['posted_date']))][] = $job;
			// if (array_key_exists($job['posted_date'], $job_dates_array)){
			// 	echo "found";
			// 	$job_dates_array[$job['posted_date']][] = $job;
			// }else{
			// 	echo "not found". $job['posted_date']."<br/>";
			// 	$job_dates_array[$job['posted_date']] = $job;
				
			// }

			//v = $job_dates_array;


			 
		}

		$results['count'] = $job_source_count;
		$results['details'] = $job_source_results;
		//echo "<pre>";print_r($results); die;
		return $results;
		


	}

	//function fetch_results($jobsector_id = '', $city_id = '', $country_id = '', $education_id = '', $occupation_id = '', $organization_id = '' ){
	function fetch_results($params ){

		//print_r($params);
		$base_query = "SELECT * FROM jobs as j ";

		// JOB SECTOR APPEND
		$base_query .= " INNER JOIN jobsector as js  ON js.jobsector_id = j.jobsector_id ";
		$base_query .= " INNER JOIN country as co  ON co.country_id = j.country_id ";
		$base_query .= " INNER JOIN organization  ON organization.organization_id = j.organization_id ";

		$where = " WHERE jobs_status = 1 ";
		if (isset($params['search_job_key_word']) &&!empty($params['search_job_key_word'])){
			
			$where.= " AND j.job_title  LIKE '%".$params['search_job_key_word']."%' or organization_name LIKE '%".$params['search_job_key_word']."%'";
		}

		
		if (!empty($params['jobsector_id']) ){
			
			$where.= " AND js.jobsector_id =".$params['jobsector_id'];
		}

		if (!empty($params['city_id']) ){

			$base_query .= " INNER JOIN jobs_cities as jc  ON jc.job_id = j.job_id 
							 INNER JOIN city ON jc.city_id = city.city_id";
			$where.= " AND j.city_id =".$params['city_id'];
		}

		if (!empty($params['qualification_id']) ){

			$base_query .= " INNER JOIN education as q  ON q.education_id = j.education_id ";
			$where.= " AND q.education_id =".$params['qualification_id'];
		}

		if (!empty($params['jobsource_id']) ){

			$base_query .= " INNER JOIN jobsource as jso  ON jso.jobsource_id = j.jobsource_id ";
			$where.= " AND j.jobsource_id =".$params['jobsource_id'];
		}

		if (!empty($params['occupation_id']) ){

			$base_query .= " INNER JOIN occupation as o  ON o.occupation_id = j.occupation_id ";
			$where.= " AND j.occupation_id =".$params['occupation_id'];
		}

		if (!empty($params['organization_id']) ){
			
			$where.= " AND j.organization_id =".$params['organization_id'];
		}


		if (!empty($params['salaryrange_id']) ){
			$base_query .= " INNER JOIN salaryrange as sr ON sr.salaryrange_id = j.salaryrange_id ";
			$where.= " AND j.salaryrange_id =".$params['salaryrange_id'];
		}

		if (!empty($params['country_id']) ){
			$base_query .= " INNER JOIN country as co ON co.country_id = co.country_id ";
			$where.= " AND j.country_id =".$params['country_id'];
		}

		

		if (!empty($params['experience_id']) ){
			$base_query .= " INNER JOIN experience as e ON e.experience_id = j.experience_id ";
			$where.= " AND j.experience_id =".$params['experience_id'];
		}


		if (!empty($params['age_id']) ){
			$base_query .= " INNER JOIN age as a ON a.age_id = j.age_id ";
			$where.= " AND j.age_id =".$params['age_id'];
		}

		if (!empty($params['gender']) ){
			$where.= " AND j.gender ='".$params['gender']."'";
		}

		if (!empty($params['jobgroup_id']) ){
			$base_query .= " INNER JOIN jobgroup as jg ON jg.jobgroup_id = j.jobgroup_id ";
			$where.= " AND j.age_id =".$params['age_id'];
		}







			 
		//$base_query.= !empty($params['city_id']) ? " INNER JOIN city as c  ON c.city_id = j.city_id " : "";
		//$base_query.= !empty($params['qualification_id']) ? " INNER JOIN education as e  ON e.education_id = j.education_id " : ""; 
		//$base_query.= !empty($params['occupation_id']) ? " INNER JOIN occupation as o  ON o.occupation_id = j.occupation_id " : "";
		//$base_query.= !empty($params['organization_id']) ? " INNER JOIN organization as org  ON org.organization_id = j.organization_id " : "";
		//echo $base_query.$where;
		$results = $this->db->query($base_query.$where);
		return $results->result_array();
	}


}
?>