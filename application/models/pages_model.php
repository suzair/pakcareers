<?php
class pages_model extends CI_Model{
	
	public $validationrules;
	public $validationrulesoccupation;
	
	function __construct()
	{
			parent::__construct();
			$this->validationrules=array(
				array(
                     'field'   => 'page_title', 
                     'label'   => 'Page TItle', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				array(
                     'field'   => 'page_text', 
                     'label'   => 'Page Description', 
                     'rules'   => 'trim|required|xss_clean'
                  )			 
             
			);
			
	}
	public function fetchActive()
	{
		
		$data=$this->general_model->selectRecord("*","pages",
														"pages_status=1","page_title","","");
		//echo '<pre>'; print_r($data->result_array()); die;
		return $data;
		
	}
	
	public function fetchInActive()
	{
		
		$data=$this->general_model->selectRecord("*","pages 
														
														","pages_status=0","page_title","","");
		return $data;
		
	}

	public function fetchAll($offset='',$pages="")
	{
		//echo "page".$pages;
		if($pages===""){
			//echo "FdfD";
			$limit="";
		}
		else{
		//	echo "33";
			$limit=$pages*$offset.",".$offset;
		}
			
		//echo "limit:".$limit."<br/>";
		$data=$this->general_model->selectRecord("*","pages","","page_title",$limit,"");
			
		return $data;		
		
	}
	
	public function deActivePage($id)
	{		
		if($this->general_model->deactiveteEntry("pages","page_id=".$id))return true; 		
	}
	public function ActivePages($id)
	{
		
		if($this->general_model->activeteEntry("pages","page_id=".$id))	return true;
		
		
	}
	
	public function getpagesById($id)
	{
		$recentnews=$this->general_model->selectRecord("*","pages","page_id=?","","",array($id));
		return $recentnews->row();		
	}
}
?>