<?php
class recentnews_model extends CI_Model{
	
	public $validationrules;
	public $validationrulesoccupation;
	
	function __construct()
	{
			parent::__construct();
			$this->validationrules=array(
				array(
                     'field'   => 'news_title', 
                     'label'   => 'New TItle', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
				array(
                     'field'   => 'news_description', 
                     'label'   => 'News Description', 
                     'rules'   => 'trim|required|xss_clean'
                  )			 
             
			);
			
	}
	public function fetchActive()
	{
		
		$data=$this->general_model->selectRecord("*","recent_news",
														"recent_news_status=1","news_title","","");
		//echo '<pre>'; print_r($data->result_array()); die;
		return $data;
		
	}
	
	public function fetchInActive()
	{
		
		$data=$this->general_model->selectRecord("*","recent_news 
														
														","recent_news_status=0","news_title","","");
		return $data;
		
	}

	public function fetchAll($offset='',$pages="")
	{
		//echo "page".$pages;
		if($pages===""){
			//echo "FdfD";
			$limit="";
		}
		else{
		//	echo "33";
			$limit=$pages*$offset.",".$offset;
		}
			
		//echo "limit:".$limit."<br/>";
		$data=$this->general_model->selectRecord("*","recent_news","","news_title",$limit,"");
			
		return $data;		
		
	}
	
	public function deActiveNews($id)
	{		
		if($this->general_model->deactiveteEntry("recent_news","news_id=".$id))return true; 		
	}
	public function Activerecentnews($id)
	{
		
		if($this->general_model->activeteEntry("recent_news","news_id=".$id))	return true;
		
		
	}
	
	public function getrecentnewsById($id)
	{
		$recentnews = $this->general_model->selectRecord("*","recent_news","news_id=?","","",array($id));
		return $recentnews->row();
		
	}
}
?>