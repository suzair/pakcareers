<?php
class search_model extends CI_Model{
	
	public $validationrules;
	
	function __construct()
	{
			parent::__construct();
			$this->validationrules=array(
				array(
                     'field'   => 'studytrack_title', 
                     'label'   => 'Study Track Title', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
              
             
			);
	}
	
	
	public function get_search_records($field, $table, $search_criteria)
	{
		$search_results = $this->general_model->getMatchString($field, $table, $search_criteria);
		return $search_results->result_array();
		
	}
	
	public function get_course_records($field, $table, $search_criteria)
	{
		$data=$this->general_model->selectRecord("*","course as j
														LEFT JOIN studytrack as st 
														 		     ON st.studytrack_id=j.studytrack_id
														 LEFT JOIN city as c
														 			ON j.city_id=c.city_id
														",$field." LIKE '%".$search_criteria."%'",$field,"","");
			
		return $data;	
		
	}
	
	function get_career_records($field, $table, $search_criteria)
	{
		$data=$this->general_model->selectRecord("*","career as j
														LEFT JOIN occupation as o 
														 		     ON o.occupation_id=j.occupation_id
														 LEFT JOIN occupationgroup as og
														 			ON j.occupationgroup_id=og.occupationgroup_id
														",$field." LIKE '%".$search_criteria."%'",$field,"","");
			
		return $data;
	}
	
	function get_jobs_records($field, $table, $search_criteria)
	{
		$data=$this->general_model->selectRecord("*","jobs as j
														 LEFT JOIN jobsector as js 
														 		     ON js.jobsector_id=j.jobsector_id
														 LEFT JOIN city as c
														 			ON j.city_id=c.city_id
														LEFT JOIN jobsource 
														 			ON j.jobsource_id=jobsource.jobsource_id
														",$field." LIKE '%".$search_criteria."%'",$field,"","");
			
		return $data;	
		
	}
}
?>