<?php
class settings_model extends CI_Model{
	
	public $validationrules;
	
	function __construct()
	{
			parent::__construct();
			$this->validationrules=array(
				array(
                     'field'   => 'facebook', 
                      'label'   => 'Facebook Title Required', 
                      'rules'   => 'trim|required|xss_clean'
                   ),
              
             
			);
	}
	
	
	
	public function getsettings()
	{

		$settings = $this->general_model->selectRecord("*","settings","","","","");
		//$settings_array = array($settings->row() , $settings);
		
		return $settings->row();
	}
}
?>