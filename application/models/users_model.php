<?php
class users_model extends CI_Model{
	
	public $validationrules;
	
	function __construct()
	{
			parent::__construct();
			$this->validationrules=array(
				array(
                     'field'   => 'username', 
                     'label'   => 'Username', 
                     'rules'   => 'trim|required|xss_clean'
                  ),
               array(
                     'field'   => 'password', 
                     'label'   => 'Password', 
                     'rules'   => 'trim|required|xss_clean'
                  )
             
			);
	}
	public function fetchActive()
	{		
		return  $this->general_model->selectRecord("*","management","user_status=1","username","","");
	}
	public function fetchInActive()
	{		
		$data=$this->general_model->selectRecord("*","management","user_status=0","username","","");
		return $data;
		
	}
	public function fetchAll()
	{
		
		//echo md5($password);
		$data=$this->general_model->selectRecord("*","management","","username","","");
		return $data;
		
	}
	
	public function deActiveusers($id)
	{
		//echo "Fdfd";
		if($this->general_model->deactiveteEntry("management","user_id=".$id))
		{
			//echo "Fdfd";
				return true;
		}
		
	}
	public function Activeusers($id)
	{
		
		if($this->general_model->activeteEntry("management","user_id=".$id))
		{
			
				return true;
		}
		
	}
	
	public function getusersById($id)
	{
		$users=$this->general_model->selectRecord("*","management","user_id=?","","",array($id));
		//echo '<pre>';
		//print_r($users); 
		return $users->row();
		
	}
}
?>