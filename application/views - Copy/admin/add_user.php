     
      <!-- Menu Toggle on mobile -->
      <button type="button" class="btn btn-navbar main"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <div class="separator bottom"></div>
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url()?>welcome" class="glyphicons home"><i></i> Dashboard</a></li>
        <li class="divider"></li>
        <li>Users</li>
        <li class="divider"></li>
        <li><?php echo isset($subtitle)?$subtitle:""?></li>
      </ul>
      <div class="separator bottom"></div>
      <h3 class="glyphicons show_thumbnails_with_lines"><i></i><?php echo $subtitle?></h3>
      <form class="form-horizontal" action="<?php echo base_url()?>users/add/<?php echo isset($user_id)?$user_id:""?>" method="post" style="margin-bottom: 0;" id="validateCountryForm" method="get" autocomplete="off" method="post" enctype="multipart/form-data">
        <h4>Please Fill Following Information Carefully</h4>
        <div class="row-fluid">
        <?php if(isset($error)){ ?>
        	<div class="error" style="background:red; color:white; padding:10px;">
            	<?php
						echo ($error);
				?>
				
            </div>
           <?php } ?>
		 <?php if($this->session->userdata("message") != ""){ ?>
        	<div class="error" style="background:green; color:white; padding:10px;">
            	<?php
						//echo urldecode($message);
						echo $this->session->userdata("message");
						$this->session->set_userdata("message","")
				?>
				
            </div>
           <?php } ?>
        </div>
        <hr class="separator line" />
        <div class="row-fluid">
          <div class="span11">
            <div class="control-group">
              <label class="control-label" for="firstname">Username</label>
               <!--<input class="span12" id="user_id" name="user_id" type="hidden" value="<?php echo set_value('user_id', isset($user_id)?$user_id:""); ?>"/>-->
              <div class="controls">
                <input class="span12" id="username" name="username" type="text" value="<?php echo set_value('username', isset($username)?$username:""); ?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="lastname">Name</label>
              <div class="controls">
                <input class="span12" id="name" name="name" type="text" value="<?php echo set_value('name', isset($name)?$name:""); ?>" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="username">Password</label>
              <div class="controls">
                <input class="span12" id="password" name="password" type="password" />
                
              </div>
            </div>
             <div class="control-group">
              <label class="control-label" for="username">Rank</label>
              <div class="controls">
                <?php echo form_dropdown('rank', array("super_admin"=>"Super Admin",
														"admin"=>"Admin",
														"content_editor"=>"Content Editor")
																
											, isset($rank)?$rank:"",'id="rank"'); ?>
                
              </div>
            </div>
          </div>
          
        </div>
        <hr class="separator line" />
        
        <div class="separator"></div>
        <div class="form-actions">
          <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
          <!--<button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>-->
        </div>
      </form>
     <h3 class="glyphicons show_thumbnails"><i></i> Existing Countries</h3>
          <div class="widget widget-4 widget-body-white">
            
            <div class="widget-body" style="padding: 10px 0 0;">
                <table class="table table-bordered table-primary table-condensed">
                    <thead>
                        <tr>
                            <th class="center">Name.</th>
                           <!-- <th>Country Code</th>
                            <th>Logo</th>-->
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    <tr>
					<th class="center" onclick=""><input class="search_user_field" type="text" name="management" id="country_search" /></th>
					
                   
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody class="data-section">
                        <?php 
                            if(count($users->result_array())>0)
                            {
                                $sr=1;
                                foreach($users->result_array() as $row)
                                {
                                    
                                    ?>
                                        <tr>
                                            
                                            <td><?php echo ucfirst(strtolower($row['name']))?></td>
                                          
                                            <td><?php echo $row['user_status']==1?"Active":"De-Active"?></td>
                                             <td>
                                                <a href="<?php echo base_url()?>users/edit/<?php echo $row['user_id']?>">Edit</a> | 
                                               
                                                <a href="<?php echo base_url()?>users/delete/<?php echo $row['user_id']?>">Delete</a>
                                                
                                             </td>
                                        </tr>
                                    <?php
                                }
                                
                            }
                        ?>
                        
                        
                    </tbody>
                </table>
            </div>
</div>
