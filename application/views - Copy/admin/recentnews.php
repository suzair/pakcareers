     
      <!-- Menu Toggle on mobile -->
      <button type="button" class="btn btn-navbar main"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <div class="separator bottom"></div>
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url()?>welcome" class="glyphicons home"><i></i> Dashboard</a></li>
        <li class="divider"></li>
        <li>News</li>
        <li class="divider"></li>
        <li><?php echo isset($subtitle)?$subtitle:""?></li>
      </ul>
      <div class="separator bottom"></div>
      <h3 class="glyphicons show_thumbnails_with_lines"><i></i><?php echo $subtitle?></h3>
      <form class="form-horizontal" action="<?php echo base_url()?>content/recentnews/add/<?php echo isset($news_id)?$news_id:""?>" method="post" style="margin-bottom: 0;" id="validateOccupationForm" method="get" autocomplete="off" method="post" enctype="multipart/form-data">
        <h4>Please Fill Following Information Carefully</h4>
        <div class="row-fluid">
            <?php echo validation_errors('<div class="error">', '</div>'); ?>
         
        <?php if(isset($error)){ ?>
        	<div class="error" style="background:red; color:white; padding:10px;">
            	
              <?php
						echo ($error);
				?>
				
            </div>
           <?php } ?>
		 <?php if(isset($message)){ ?>
        	<div class="error" style="background:green; color:white; padding:10px;">
            	<?php
						echo ($message);
				?>
				
            </div>
           <?php } ?>
        </div>
        <hr class="separator line" />
        <input class="span12" id="news_date" name="news_date" type="hidden" value="<?php echo set_value('news_date', isset($news_date) ? $news_date: date("Y-m-d h:i:s")); ?>"/>
        <div class="row-fluid">
        <input class="span12" id="news_id" name="news_id" type="hidden" value="<?php echo set_value('news_id', isset($news_id)?$news_id:""); ?>"/>
          <div class="span11">
          	<div class="control-group">
              <label class="control-label" for="firstname">Title</label>
              
              <div class="controls">
                <input class="span12" id="news_title" name="news_title" type="text" value="<?php echo set_value('news_title', isset($news_title) ? $news_title:""); ?>"/>
              </div>
            </div>
             
            <div class="control-group">
              <label class="control-label" for="firstname">Description</label>
              
              <div class="controls">
              <textarea class="span12" name="news_description" id="news_description"><?php echo isset($news_description) ? $news_description:""?></textarea>
              </div>
            </div>
        </div>
        <hr class="separator line" />
        
        <div class="separator"></div>
        <div class="form-actions">
          <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
          <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
        </div>
      </form>

<h3 class="glyphicons show_thumbnails"><i></i> Existing Entries</h3>
  <div class="widget widget-4 widget-body-white">
  
  <div class="widget-body" style="padding: 10px 0 0;">
    <table class="table table-bordered table-primary table-condensed">
      <thead>
        <tr>
          <th class="center" onclick="sort_career_track()">News Title</th>
          
                   
          <th onclick="sort_career_track_status()">Status</th>
          <th>Action</th>
        </tr>
                <tr>
          <th class="center" onclick="">
           <!-- <input class="search_field" type="text" name="recent_news" id="news_title" /> -->
          </th>
          
                   
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody class="data-section">
              <?php 

          if(count($recentnews->result_array())>0)
          {
            $sr=1;
            foreach($recentnews->result_array() as $row)
            {
              
              ?>
                              <tr>
                                    
                                    <td><?php echo ucfirst(strtolower($row['news_title']))?></td>
                                   
                                 
                                    <td><?php echo $row['recent_news_status']==1?"Active":"De-Active"?></td>
                                     <td>
                                      
                                      <a href="<?php echo base_url()?>content/recentnews/edit/<?php echo $row['news_id']?>">Edit</a> | 
                                        <?php if($row['recent_news_status']==1){ ?>
                                        <a href="<?php echo base_url()?>content/recentnews/delete/<?php echo $row['news_id']?>">De Activate</span></a>
                                        <?php } else { ?>
                                           <a href="<?php echo base_url()?>content/recentnews/active/<?php echo $row['news_id']?>">Active</span></a>
                                        <?php } ?>
                                     </td>
                                </tr>
                            <?php
            }
            
          }
        ?>
        
        
      </tbody>
    </table>
  </div>
</div>
