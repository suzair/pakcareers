
      <!-- Menu Toggle on mobile -->
      <button type="button" class="btn btn-navbar main"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <div class="separator bottom"></div>
      <div class="heading-buttons">
        <h2>Dashboard<span class="hidden-phone">| Welcome to PakCareers </span></h2>
        <div class="buttons pull-right">  </div>
        <div class="clearfix"></div>
      </div>
      
      <div class="separator bottom"></div>
      <div class="widget-head">
              <h4 class="heading">JOBS</h4>
            </div>
      <div class="span3">
      	<div id="jobs_by_gender"></div>
      </div>
       <div class="span3">
      	<div id="jobs_by_source"></div>
      </div>
       <div class="span3">
      	<div id="jobs_by_city"></div>
      </div>
       <div class="span3">
      	<div id="jobs_by_views"></div>
      </div>
      <div class="clear"></div>
      
      <div class="separator bottom line"></div>
      
     
      
      <div class="separator line"></div>
      
      
      <?php
	  	
		
	  ?>
      <script>
$(function () {
	drawdonut("#jobs_by_gender" ,"Jobs By Gender","Jobs By Gender", [<?php echo join($jobs_by_gender,",")?>],false);
	drawdonut("#jobs_by_source" ,"Jobs By Job Source","Jobs By Job Source", [<?php echo join($jobs_by_job_source,",")?>],false);
	drawdonut("#jobs_by_city" ,"Jobs By City","Jobs By City", [<?php echo join($jobs_by_job_city,",")?>],false);
	
	

});

	  </script>