<div class="outer-wrap footer-bg">
    <div class="inner-wrap">
      <div class="footer-col">
        <h5>Career Resource</h5>
        <p>Lorem ipsum dolor sit amet  consectetur adipiscing elit. odio interdum tellus, et laci odio interdum tellus, et lacin.</p>
        <a href="<?php echo $settings->facebook?>" title="Facebook" class="facebook-icon"></a> <a href="<?php echo $settings->twitter?>" title="Twitter" class="twitter-icon"></a> <a href="<?php echo $settings->linked_in?>" title="linkedin" class="linkedin-icon"></a> </div>
      <div class="footer-col">
        <h5>About us</h5>
        <ul>
          <li><a href="#" title="Search by Qualification">Search by Qualification</a></li>
          <li><a href="#" title="Search by Newspaper">Search by Newspaper</a></li>
          <li><a href="#" title="Search by Date">Search by Date</a></li>
          <li><a href="#" title="Search by profession">Search by profession</a></li>
          <li><a href="#" title="Search by sector">Search by sector</a></li>
        </ul>
      </div>
      <div class="footer-col">
        <h5>Help</h5>
        <ul>
          <li><a href="#" title="Contact us">Contact us</a></li>
          <li><a href="#" title="Support">Support</a></li>
          <li><a href="#" title="Faq">Faq</a></li>
        </ul>
      </div>
      <div class="footer-col">
        <h5>facebook</h5>
        <img src="<?php echo base_url()?>files/front/images/facebook.png" width="168" height="115" alt="facebook" /> </div>
      <div class="footer-col nobdr-spacing" style="width:220px;">
        <h5>Contact Info</h5>
        <div class="address-row">
          <div class="icon"><img src="<?php echo base_url()?>files/front/images/address.png" width="35" height="35" alt="Address" /></div>
          <div class="text"><?php echo $settings->address?></div>
        </div>
        <div class="address-row">
          <div class="icon"><img src="<?php echo base_url()?>files/front/images/phone.png" width="35" height="35" alt="phone" /></div>
          <div class="text top-space"><?php echo $settings->contact_number?></div>
        </div>
        <div class="address-row">
          <div class="icon"><img src="<?php echo base_url()?>files/front/images/phone.png" width="35" height="35" alt="phone" /></div>
          <div class="text top-space"><a href="mailto:<?php echo $settings->email?>" title="email"><?php echo $settings->email?></a></div>
        </div>
      </div>
    </div>
  </div>
  <div class="outer-wrap copyright-bg">
    <div class="inner-wrap">
      <div class="pull-left">Copyright&copy; 2012. All Rights Reserved. <a href="#" target="_blank" title="www.careerpak.com">www.careerpak.com</a></div>
      <div class="pull-right"><a href="#top" title="go to top">^ Back to Top</a></div>
    </div>
  </div>
  <div class="shadow">
  <div class="loader">
      <img src="<?php echo base_url()?>files/images/loader.gif" />
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url()?>files/js/custom.js"></script>

</script>