<div class="sidebar">
  <h2>Todays Occupation</h2>
  <span>We provide following services</span>
  <div class="listing">
    <ul>
      <li><a href="#">Lorem ipsum dolor sit amet</a></li>
      <li><a href="#">Suspendisse dictum mauris </a></li>
      <li><a href="#">Nullam rhoncus interdum</a></li>
    </ul>
    <a href="#">Read more...</a> </div>
  <div class="latest-news">
    <h2>Latest News</h2>
    <?php
      if(count($latest_news->result_array()) > 0){
        foreach($latest_news->result_array() as $news){
          ?>
          <div class="news-row">
            <div class="date"><?php echo date_with_month_and_days($news['news_date'])?></div>
            <div class="newstext">
              <h4><?php 
              echo $news['news_title']?></h4>
              <span><?php echo $news['news_description']?> </span></div>
          </div>
          <?php     
        }
      }
    ?>
    
    
  </div>
  <a href="#">View all...</a> 
  </div>