<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
$this->load->view("head");
?>
</head>

<body>

<!-- Wrapper start -->

<div class="outer-wrap">
  <div class="green-bg"> 
    <!-- Header start -->
    <div class="inner-wrap">
      <?php $this->load->view("header"); ?>
    </div>
    <!-- Header end --> 
  </div>
  <!-- content section -->
  <?php  $this->load->view($view); ?> 
  
  <!-- footer start -->
    <?php $this->load->view("footer"); ?>  
  <!-- footer end --> 
  
</div>
<!-- Wrapper end --> 
</body>
</html>
