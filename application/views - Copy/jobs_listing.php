
   <!-- Container start -->
   <div class="inner-wrap">
   	<div class="content" style="padding:0;">
    	
        <!-- Side bar start -->
        <div class="left-side">
          <form action="<?php echo base_url()?>/jobs/jobs_results" method="post">
           
           <ul class="menu">
                <li>
                    <a href="#">Search job by Date posted <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>                         
                         <?php echo form_dropdown('job_posted_date', $job_posted_date, "",'id="job_posted_date" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                        
                    </ul>
                </li>
          </ul>

           <ul class="menu">
                <li>
                    <a href="#">Search job by salary range <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>

                         <?php echo form_dropdown('salaryrange_id', $salary_ranges, "",'id="salaryrange_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                        
                    </ul>
                </li>
	        </ul>

          <ul class="menu">
                <li>
                    <a href="#">Search job by Organization <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>
                         <?php echo form_dropdown('organization_id', $organizations, "",'id="organization_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                        
                    </ul>
                </li>
          </ul>
            
            
          <div class="clear"></div>    

           <ul class="menu">
                <li>
                    <a href="#">Search job by Job Sector <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    <ul>                                               
                         <?php echo form_dropdown('jobsector_id', $job_sectors, "",'id="jobsector_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                        
                    </ul>
                </li>
          </ul> 

          <div class="clear"></div>    

           <ul class="menu">
                <li>
                    <a href="#">Search job by Qualifications <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    <ul>                                               
                         <?php echo form_dropdown('qualification_id', $educations, "",'id="qualification_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                        
                    </ul>
                </li>
          </ul>       
           <ul class="menu">
                <li>
                    <a href="#">Search job by sector <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>
                        <span>Government job or private job</span>
                      
                <div class="combowrap">
                    <select class="comboitems" name="combolist">
                    <option selected="selected">Select Salary Range</option>
                    <option value="1">Option 1</option>
                    <option value="2">Option 2</option>
                    <option value="3">Option 3</option>
                    <option value="4">Option 4</option>
                    </select>
                   </div>
               
                    </ul>
                </li>
          </ul> 
            
           <div class="clear"></div>           
           <ul class="menu">
                <li>
                    <a href="#">Search job by qualification <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>
                        <span>Government job or private job</span>
                                       	
                            <div class="combowrap">
                                <select class="comboitems" name="combolist">
                                <option selected="selected">Select sector</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                                <option value="4">Option 4</option>
                                </select>
                           </div>
                        
                    </ul>
                </li>
          </ul> 
            
            
            <div class="clear"></div>           
           <ul class="menu">
                <li>
                    <a href="#">Search job by gender preferrence <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>
                        <span>Search job by selecting the level of eduction from the list job</span>
                                    	
                            <div class="combowrap">
                                <select class="comboitems" name="combolist">
                                <option selected="selected">Select sector</option>
                                <option value="1">Option 1</option>
                                <option value="2">Option 2</option>
                                <option value="3">Option 3</option>
                                <option value="4">Option 4</option>
                                </select>
                           </div>
                        
                    </ul>
                </li>
          </ul> 
            
            
             <div class="clear"></div>           
           <ul class="menu">
                <li>
                    <a href="#">Search job by education level <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>
                        <span>Search job by selecting the level of eduction from the list job</span>
                                       	
                            <div class="combowrap">
                            <select class="comboitems" name="combolist">
                            <option selected="selected">Select sector</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                            <option value="4">Option 4</option>
                            </select>
               			</div>
                        
                    </ul>
                </li>
          </ul> 
      </form>
             
    </div>
        <!-- Side bar end -->
        
        <!-- Main Content Start -->
  <div class="main-content" style="width:665px;">
         	<h2>Find Job</h2>            
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ultricies laoreet elit, commodo venenatis ante molestie quis</p>
        <div class="main-content-ad">Ad here</div>
        <div class="clear"></div>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="grid">
            <tr>
              <th colspan="5">Your Search Result</th>      
            </tr>
            <tr>
              <td>Newspaper</td>
              <td>Job Title</td>
              <td>Company</td>
              <!--<td>Location</td>
              <td>Sector</td>-->
            </tr>
            <?php
              if (count($results) > 0 ){
                foreach($results as $result){
                    ?>
                      <tr>
                          <td><img src="<?php echo base_url()?>uploads/<?php echo $result['jobsource_logo']?>" width="49" height="37" alt="newspaper name" /></td>
                          <td><?php echo $result['job_title']?></td>
                          <td><img src="<?php echo base_url()?>uploads/<?php echo $result['organization_logo']?>" width="77" height="30" /></td>
                          
                        </tr>
                    <?php     
                }
              }
            ?>
            
          </table>

           
      </div>
        <!-- Main Content End -->
       
        
        
    </div>
   </div>
   <!-- Container end -->
   
  