     
      <!-- Menu Toggle on mobile -->
      <button type="button" class="btn btn-navbar main"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <div class="separator bottom"></div>
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url()?>welcome" class="glyphicons home"><i></i> Dashboard</a></li>
        <li class="divider"></li>
        <li>Settings</li>
        <li class="divider"></li>
        <li><?php echo isset($subtitle)?$subtitle:""?></li>
      </ul>
      <div class="separator bottom"></div>
      <h3 class="glyphicons show_thumbnails_with_lines"><i></i><?php echo $subtitle?></h3>
      <form class="form-horizontal" action="<?php echo base_url()?>settings/general/add" method="post" style="margin-bottom: 0;" id="validateOccupationForm" method="get" autocomplete="off" method="post" enctype="multipart/form-data">
        <h4>Please Fill Following Information Carefully</h4>
        <div class="row-fluid"> <?php echo validation_errors('<div class="error">', '</div>'); ?></div>
        <div class="row-fluid">
        <?php if(isset($error)){ ?>
        	<div class="error" style="background:red; color:white; padding:10px;">
            	<?php
						echo ($error);
				?>
				
            </div>
           <?php } ?>
		 <?php if(isset($message)){ ?>
        	<div class="error" style="background:green; color:white; padding:10px;">
            	<?php
						echo ($message);
				?>
				
            </div>
           <?php } ?>
        </div>
        <hr class="separator line" />
        <div class="row-fluid">
        
          <div class="span11">
          	<div class="control-group">
              <label class="control-label" for="firstname">Facebook</label>
              
              <div class="controls">
                
                <input class="span12" id="facebook" name="facebook" type="text" value="<?php echo set_value('facebook', isset($facebook)? $facebook:""); ?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="twitter">Twitter</label>
              
              <div class="controls">
                <input class="span12" id="twitter" name="twitter" type="text" value="<?php echo set_value('twitter', isset($twitter)?$twitter:""); ?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="youtube">Youtube</label>
              
              <div class="controls">
                <input class="span12" id="youtube" name="youtube" type="text" value="<?php echo set_value('youtube', isset($youtube)?$youtube:""); ?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="pin_intereset">Pin Interest</label>
              
              <div class="controls">
                <input class="span12" id="pin_intereset" name="pin_intereset" type="text" value="<?php echo set_value('pin_intereset', isset($pin_intereset)?$pin_intereset:""); ?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="linked_in">Linked IN</label>
              
              <div class="controls">
                <input class="span12" id="linked_in" name="linked_in" type="text" value="<?php echo set_value('linked_in', isset($linked_in)?$linked_in:""); ?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="contact_number">Contact Number</label>
              
              <div class="controls">
                <input class="span12" id="contact_number" name="contact_number" type="text" value="<?php echo set_value('contact_number', isset($contact_number)?$contact_number:""); ?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="mobile_no">Mobile No</label>
              
              <div class="controls">
                <input class="span12" id="mobile_no" name="mobile_no" type="text" value="<?php echo set_value('mobile_no', isset($mobile_no)?$mobile_no:""); ?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="email">Email</label>
              
              <div class="controls">
                <input class="span12" id="email" name="email" type="text" value="<?php echo set_value('email', isset($email)?$email:""); ?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="admin_email">Admin Email</label>
              
              <div class="controls">
                <input class="span12" id="admin_email" name="admin_email" type="text" value="<?php echo set_value('admin_email', isset($admin_email)?$admin_email:""); ?>"/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="address">Address</label>
              
              <div class="controls">
                <textarea name="address"><?php echo set_value('address', isset($address)?$address:""); ?></textarea>
                
              </div>
            </div>
              <div class="control-group">
                  <label class="control-label" for="logo">logo</label>              
                  <div class="controls">
                   <input type="file" name="logo" id="logo" />
                  </div>
              </div>   
              
            
            
          </div>
          
        </div>
        <hr class="separator line" />
        
        <div class="separator"></div>
        <div class="form-actions">
          <button type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
          <button type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Cancel</button>
        </div>
      </form>
  
