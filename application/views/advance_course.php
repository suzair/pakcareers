
<script src="<?php echo base_url()?>files/front/js/jquery-ui.min.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/files/css/calendar.css">
 <div class="outer-wrap">
 <div class="inner-wrap">
   	<div class="content">
    	<div class="inner-wrap2">
            <h2>Find Courses</h2>
            <p>Find Courses according to your criteria by using following search tools.</p>
            
           
            <div class="right-wrap advance-jo-section">
            	<div class="right-wrap-inner">
                     <h2>Find Course By Degree / Institution</h2>
                    <form action="<?php echo base_url()?>/course/course_results" method="post">
                    	<?php echo form_dropdown('course_degrees', $degree_programs, "",'id="course_degrees1"' ,'class="course_degrees1"'); ?>
                        <?php echo form_dropdown('city_id', $cities, "",'id="city_id"',  'class="comboitems"'); ?>
                        <input name="" type="submit" class="pull-right btn-blue2" value="Search" /> 
                       </form>
                    <br/>
                     <br>
<br>
                      <h2>Find Course By Field of Education</h2>
                    <form action="<?php echo base_url()?>/course/course_results" method="post">
                         <!-- <div class="combowrap">-->
                        
                            <?php echo form_dropdown('field_of_education', $field_of_educations, "",'id="field_of_education"',  'class="comboitems"'); ?>
                            <?php echo form_dropdown('level_of_education', $level_of_educations, "",'id="level_of_education"',  'class="comboitems"'); ?>
                            <?php echo form_dropdown('city_id', $cities, "",'id="city_id1"',  'class="comboitems"'); ?>

              <!--          
                        </div>-->
                        <input name="" type="submit" class="pull-right btn-blue2" value="Search" /> 
                       
                    </form>

                    <br/>
                    <br>
                    <br>
                     <h2>Find Job By Job Sector</h2>
                    
                   
            </div>
            <div class="clear"></div>
            <br />
            <div class="right-wrap">
           	 
              <div class="right-wrap-inner">
              
                
                </div>
            </div>
        </div>
        
        <!-- Right bar -->
        <div class="right-bar">
       	  <div class="right-ad">Add here</div>
            <div class="right-video"><a href="#"><img src="<?php echo base_url()?>files/front/images/video_thumb.jpg" width="195" height="112" alt="video title" border="0" /></a></div>
            
           <a href="#"> <div class="grey-box">
            	<div class="icon">
                	<img src="<?php echo base_url()?>files/front/images/degree_icon2.png"  alt=" " />                    
                </div>
                <h3>Find related degree/training in Pakistan</h3>
            </div>
            </a>
            
           <a href="#"> 
                <div class="grey-box">
                    <div class="icon"><img src="<?php echo base_url()?>files/front/images/career_icon.png" alt=" " />    </div>
                    <h3 style="margin-top:15px;">Find related career guidance</h3>
                </div>
            </a>
            
           <a href="#"> 
                <div class="grey-box">
                    <div class="icon"><img src="<?php echo base_url()?>files/front/images/search_icon2.png" alt=" " />    </div>
                    <h3 style="margin-top:12px;">Find related <br />Job</h3>
                </div>
            </a>
             <div class="right-ad">Add here</div>
        </div>
        
    </div>
   </div>
   </div>
   <!-- Container end -->
   <script type="text/javascript">
  $("#level_of_education").select2();
  $("#field_of_education").select2();
  $("#level_of_education").select2();
  $("#city_id1").select2();
  $("#course_degrees1").select2();
   </script>