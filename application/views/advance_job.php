
<script src="<?php echo base_url()?>files/front/js/jquery-ui.min.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/files/css/calendar.css">
 <div class="outer-wrap">
 <div class="inner-wrap">
   	<div class="content">
    	<div class="inner-wrap2">
            <h2>Find Job</h2>
            <p>Find Jobs according to your criteria by using following search tools.</p>
            
           
            <div class="right-wrap advance-jo-section">
            	<div class="right-wrap-inner">
                     <h2>Find Job By Qualification</h2>
                    <form action="<?php echo base_url()?>/jobs/jobs_results" method="post">
                    	
                        
                        
                     <!--   <div class="combowrap">-->
                            <?php echo form_dropdown('qualification_id', $educations, "",'id="qualification_id"' ,'class="comboitems"'); ?>
                           
                    <!-- </div>
                        
                        <div class="combowrap">-->
                            <?php echo form_dropdown('city_id', $cities, "",'id="city_id"',  'class="comboitems"'); ?>
                             
                     <!--   </div>-->
                        <input name="" type="submit" class="pull-right btn-blue2" value="Search" /> 
                       
                    </form>
                    <br/>
                     <br>
<br>
                      <h2>Find Job By Organization</h2>
                    <form action="<?php echo base_url()?>/jobs/jobs_results" method="post">
                         <!-- <div class="combowrap">-->
                        
                            <?php echo form_dropdown('organization_id', $organizations, "",'id="organization_id"',  'class="comboitems"'); ?>
                            
              <!--          
                        </div>-->
                        <input name="" type="submit" class="pull-right btn-blue2" value="Search" /> 
                       
                    </form>

                    <br/>
                    <br>
                    <br>
                     <h2>Find Job By Job Sector</h2>
                    <form action="<?php echo base_url()?>/jobs/jobs_results" method="post">                       
                        
                        
                        <!-- <div class="combowrap"> -->
                            <?php echo form_dropdown('jobsector_id', $job_sectors, "",'id="jobsector_id"',  'class="comboitems"'); ?>
                           
                  <!--</div>
                        
                        <div class="combowrap"> -->
                            <?php echo form_dropdown('city_id', $cities, "",'id="city_id" class="city_id"',  'class="comboitems"'); ?>
                             
                         <!--</div>--> 
                        <input name="" type="submit" class="pull-right btn-blue2" value="Search" /> 
                       
                    </form>
                    <br/>
<br><br>
                     <h2>Find Job By Occupation</h2>
                    <form action="<?php echo base_url()?>/jobs/jobs_results" method="post">                       
                        
                        
                        <!-- <div class="combowrap">--> 
                            <?php echo form_dropdown('occupation_id', $occupations, "",'id="occupation_id"',  'class="comboitems"'); ?>
                           
                       <!-- </div>
                        
                        <div class="combowrap"> -->
                            <?php echo form_dropdown('city_id', $cities, "",'id="city_id" class="city_id"',  'class="comboitems"'); ?>
                             
                         <!--</div>-->
                        <input name="" type="submit" class="pull-right btn-blue2" value="Search" /> 
                       
                    </form>
                    <br/>




                   
            </div>
            <div class="clear"></div>
            <br />
            <div class="right-wrap">
           	  <h3>Search by  Newspaper</h3>
              <div class="right-wrap-inner">
                <?php
                    if (count($job_sources->result_array()) > 0){
                        foreach ($job_sources->result_array() as $key => $value) {
                            ?>
                                <div class="calendar-wrap">

                                 <div class="newspaper-logo-calendar"><a href="#" title="<?php echo $value['jobsource_title']?>" > 
                                    <img width="60" src="<?php echo base_url()?>uploads/<?php echo $value['jobsource_logo']?>"  alt="<?php echo $value['jobsource_logo']?>" border="0" /></a></div>
                                 <div class="new-jobs"><a href="<?php echo base_url()?>jobs/jobs_results/<?php echo $value['jobsource_id']?>"><?php echo $job_calendar_results['count'][$value['jobsource_title']]?> new jobs</a></div>
                                 <?php 
                                 $jobsource_details = $job_calendar_results['details'][$value['jobsource_title']];
                                    echo draw_calendar(date('m'),date('Y'), $jobsource_details); ?>
                                
                                 
                                </div>
                            <?php
                        }
                    }
                ?>
                
                
                </div>
            </div>
        </div>
        
        <!-- Right bar -->
        <div class="right-bar">
       	  <div class="right-ad">Add here</div>
            <div class="right-video"><a href="#"><img src="<?php echo base_url()?>files/front/images/video_thumb.jpg" width="195" height="112" alt="video title" border="0" /></a></div>
            
           <a href="#"> <div class="grey-box">
            	<div class="icon">
                	<img src="<?php echo base_url()?>files/front/images/degree_icon2.png"  alt=" " />                    
                </div>
                <h3>Find related degree/training in Pakistan</h3>
            </div>
            </a>
            
           <a href="#"> 
                <div class="grey-box">
                    <div class="icon"><img src="<?php echo base_url()?>files/front/images/career_icon.png" alt=" " />    </div>
                    <h3 style="margin-top:15px;">Find related career guidance</h3>
                </div>
            </a>
            
           <a href="#"> 
                <div class="grey-box">
                    <div class="icon"><img src="<?php echo base_url()?>files/front/images/search_icon2.png" alt=" " />    </div>
                    <h3 style="margin-top:12px;">Find related <br />Job</h3>
                </div>
            </a>
             <div class="right-ad">Add here</div>
        </div>
        
    </div>
   </div>
   </div>
   <!-- Container end -->
   <script type="text/javascript">
  $("#qualification_id").select2();
   </script>