
   <!-- Container start -->
   <div class="inner-wrap">
   	<div class="content" style="padding:0;">
    	
        <!-- Side bar start -->
        <div class="left-side job-listing">
          <form action="<?php echo base_url()?>/career/career_results" method="post">
           
           <ul class="menu">
                <li>
                    <a href="#">Search Career by Date Occupation <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>                         
                         <?php echo form_dropdown('occupation_id', $occupations, isset($occupation_id) ? $occupation_id : "",'id="occupation_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                          <div class="clear"></div>
                    </ul>
                </li>
          </ul>

           <ul class="menu">
                <li>
                    <a href="#">Search Career By Job Group <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>

                         <?php echo form_dropdown('jobgroup_id', $job_groups, isset($jobgroup_id) ? $jobgroup_id : "",'id="jobgroup_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                          <div class="clear"></div>
                    </ul>
                </li>
	        </ul>

          <ul class="menu">
                <li>
                    <a href="#">Search Career by Field of Work <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>
                         <?php echo form_dropdown('field_of_work_id', $field_of_works, isset($field_of_work_id) ? $field_of_work_id : "",'id="field_of_work_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                        <div class="clear"></div>
                    </ul>
                </li>
          </ul>
            
            
        <!--   <div class="clear"></div>    

           <ul class="menu">
                <li>
                    <a href="#">Search job by Job Sector <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    <ul>                                               
                         <?php echo form_dropdown('jobsector_id', $job_sectors, isset($jobsector_id) ? $jobsector_id : "",'id="jobsector_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                        <div class="clear"></div>
                    </ul>
                </li>
          </ul>  -->

                
           
      </form>
             
    </div>
        <!-- Side bar end -->
        
        <!-- Main Content Start -->
  <div class="main-content" style="width:665px;">
         	<h2>Find Job</h2>            
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ultricies laoreet elit, commodo venenatis ante molestie quis</p>
        <div class="main-content-ad">Ad here</div>
        <div class="clear"></div>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="grid">
            <tr>
              <th colspan="5">Your Search Result</th>      
            </tr>
            <tr>
              <td>Newspaper</td>
              <td>Job Title</td>
              <td>Company</td>
              <!--<td>Location</td>
              <td>Sector</td>-->
            </tr>
            <?php
              if (isset($results)){
                foreach($results as $result){
                    ?>
                      <tr>
                          <td><img src="<?php echo base_url()?>uploads/<?php echo $result['jobsource_logo']?>" width="49" height="37" alt="newspaper name" /></td>
                          <td><?php echo $result['job_title']?></td>
                          <td><img src="<?php echo base_url()?>uploads/<?php echo $result['organization_logo']?>" width="77" height="30" /></td>
                          
                        </tr>
                    <?php     
                }
              }
            ?>
            
          </table>

           
      </div>
        <!-- Main Content End -->
       
        
        
    </div>
   </div>
   <!-- Container end -->
   
  <script type="text/javascript">
    $("#qualification_id").select2();
  </script>