
   <!-- Container start -->
   <div class="inner-wrap">
   	<div class="content" style="padding:0;">
    	
        <!-- Side bar start -->
        <div class="left-side job-listing">
          <form action="<?php echo base_url()?>/jobs/jobs_results" method="post">
           
           <ul class="menu">
                <li>
                    <a href="#">Search job by Date posted <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>                         
                         <?php echo form_dropdown('job_posted_date_id', $job_posted_date, isset($job_posted_date_id) ? $job_posted_date_id : "",'id="job_posted_date" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                          <div class="clear"></div>
                    </ul>
                </li>
          </ul>

           <ul class="menu">
                <li>
                    <a href="#">Search job by salary range <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>

                         <?php echo form_dropdown('salaryrange_id', $salary_ranges, isset($salaryrange_id) ? $salaryrange_id : "",'id="salaryrange_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                          <div class="clear"></div>
                    </ul>
                </li>
	        </ul>

          <ul class="menu">
                <li>
                    <a href="#">Search job by Organization <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>
                         <?php echo form_dropdown('organization_id', $organizations, isset($organization_id) ? $organization_id : "",'id="organization_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                        <div class="clear"></div>
                    </ul>
                </li>
          </ul>
            
            
          <div class="clear"></div>    

           <ul class="menu">
                <li>
                    <a href="#">Search job by Job Sector <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    <ul>                                               
                         <?php echo form_dropdown('jobsector_id', $job_sectors, isset($jobsector_id) ? $jobsector_id : "",'id="jobsector_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                        <div class="clear"></div>
                    </ul>
                </li>
          </ul> 

          <div class="clear"></div>    

           <ul class="menu">
                <li>
                    <a href="#">Search job by Qualifications <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    <ul>                                               
                         <?php echo form_dropdown('qualification_id', $educations, isset($qualification_id) ? $qualification_id : "",'id="qualification_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                        <div class="clear"></div>
                    </ul>
                </li>
          </ul>       
           
      </form>
             
    </div>
        <!-- Side bar end -->
        
        <!-- Main Content Start -->
  <div class="main-content" style="width:665px;">
         	<h2>Find Job</h2>            
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ultricies laoreet elit, commodo venenatis ante molestie quis</p>
        <div class="main-content-ad">Ad here</div>
        <div class="clear"></div>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="grid">
            <tr>
              <th colspan="9">Your Search Result</th>      
            </tr>
            <tr>
              
              <th>Course of Study</th>
              <th>Program</th>
              <th>Program Duration</th>              
              <th>Institution</th>
               <th>City</th>
              <th>Fee</th>
              <th>Admission Deadline</th>              
              <th>Field of Education</th>
              <th>Level of Education</th>

            </tr>
            <?php
              if (count($results) > 0 ){
                foreach($results as $result){
                    ?>
                    
                      <tr>
                          <td><a href="<?php echo base_url()?>course/course_detail/<?php echo $result['course_id']?>"><?php echo $result['course_name']?></a></td>
                          <td><?php echo $result['course_degreeprogram']?></td>
                          <td><?php echo $result['course_duration']?></td>
                          <td><?php echo $result['course_institution']?></td>
                          <td><?php echo $result['city_name']?></td>
                          <td><?php echo $result['course_fee']?></td>
                          <td><?php echo short_date($result['course_admissiondeadline'])?></td>
                          <td><?php echo $result['course_duration']?></td>
                          
                          <td><?php echo short_date($result['course_duration'])?></td>
                          <!-- <td><img src="<?php echo base_url()?>uploads/<?php echo $result['organization_logo']?>" width="77" height="30" /></td> -->
                          
                        </tr>
                    
                    <?php     
                }
              }
            ?>
            
          </table>

           
      </div>
        <!-- Main Content End -->
       
        
        
    </div>
   </div>
   <!-- Container end -->
   
  <script type="text/javascript">
    $("#qualification_id").select2();
  </script>