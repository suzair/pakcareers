 <div class="main-content">
    <h2>Latest Job</h2>
    <?php if(isset($latest_jobs)){
       foreach($latest_jobs->result_array() as $job)
       {
      ?>
      <div class="jobs-row">
        <div class="jobs-text">
            <h4><?php echo $job['job_title']?></h4>
            <div class="date">
              <img src="<?php echo base_url()?>files/front/images/calendar-icon.png" width="13" height="16" alt="date" />
              <?php echo short_date($job['posted_date'])?>
            </div>
            <p>  </p>
        </div>
          <div class="newpaper-logo"><a href="#"><img src="<?php echo base_url()?>uploads/<?php echo isset($job['jobsource_logo']) ? $job['jobsource_logo'] : ''?>" width="83" height="55" alt="Jang groups" /></a>
       </div>
      </div>
        
      <?php
      }
    }
  ?>
  </div>