<!-- Banner start -->
  <div class="banner">
    <div class="inner-wrap">
      <h2>Your Complete Career Course</h2>
      <div class="search-criteria" id="main-form">
        	<?php $this->load->view("tabs_panel"); ?>
      </div>
    </div>
  </div>
  <!-- Banner end --> 
  
  <!-- Container start -->
  <div class="inner-wrap">
    <div class="content"> 
      <!-- Feature services start -->
      <div class="feature-services">
        <?php $this->load->view("featured_services"); ?>
      </div>
      <!-- Feature services end -->
      
      <div class="shadow"><img src="<?php echo base_url()?>files/front/images/shadow.png" width="898" height="13" alt=" " /></div>
      
      <!-- Sidebar start -->
         <?php $this->load->view("front_sidebar"); ?>
      <!-- Sidebar start --> 
      
      <!-- Main content start -->
        <?php $this->load->view("front_main_content") ?>
      <!-- Main content end --> 
      
      <!-- Latest videos -->
        <?php $this->load->view("latest_video"); ?>
      <!-- Latest videos --> 
    </div>
  </div>
  <!-- Container end -->