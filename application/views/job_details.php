
   <!-- Container start -->
   <div class="inner-wrap">
   	<div class="content" style="padding:0;">
    	
        <!-- Side bar start -->
        <div class="left-side job-listing">
          <form action="<?php echo base_url()?>/jobs/jobs_results" method="post">
           
           <ul class="menu">
                <li>
                    <a href="#">Search job by Date posted <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>                         
                         <?php echo form_dropdown('job_posted_date_id', $job_posted_date, isset($job_posted_date_id) ? $job_posted_date_id : "",'id="job_posted_date" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                          <div class="clear"></div>
                    </ul>
                </li>
          </ul>

           <ul class="menu">
                <li>
                    <a href="#">Search job by salary range <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>

                         <?php echo form_dropdown('salaryrange_id', $salary_ranges, isset($salaryrange_id) ? $salaryrange_id : "",'id="salaryrange_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                          <div class="clear"></div>
                    </ul>
                </li>
	        </ul>

          <ul class="menu">
                <li>
                    <a href="#">Search job by Organization <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    
                    <ul>
                         <?php echo form_dropdown('organization_id', $organizations, isset($organization_id) ? $organization_id : "",'id="organization_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                        <div class="clear"></div>
                    </ul>
                </li>
          </ul>
            
            
          <div class="clear"></div>    

           <ul class="menu">
                <li>
                    <a href="#">Search job by Job Sector <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    <ul>                                               
                         <?php echo form_dropdown('jobsector_id', $job_sectors, isset($jobsector_id) ? $jobsector_id : "",'id="jobsector_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                        <div class="clear"></div>
                    </ul>
                </li>
          </ul> 

          <div class="clear"></div>    

           <ul class="menu">
                <li>
                    <a href="#">Search job by Qualifications <img src="<?php echo base_url()?>files/images/downarrow.png" width="9" height="5" alt="" /></a>
                    <ul>                                               
                         <?php echo form_dropdown('qualification_id', $educations, isset($qualification_id) ? $qualification_id : "",'id="qualification_id" class="js-select2-dropdown"'); ?>
                         <input name="" type="image" src="<?php echo base_url()?>files/images/search_icon3.png" class="left-search-icon" />              
                        <div class="clear"></div>
                    </ul>
                </li>
          </ul>       
           
      </form>
             
    </div>
        <!-- Side bar end -->
        
        <!-- Main Content Start -->
  <div class="main-content" style="width:665px;">
         	<h2><?php echo $job_result->job_title ?></h2>            
       
        <div class="main-content-ad">Ad here</div>
          <div class="clear"></div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="grid">
            <tr>
                <th>Job Title</th>
                <td><?php echo $job_result->job_title?></td>
            </tr>
            <tr>
                <th>Job Group</th>
                <td><?php echo $job_result->jobgroup_title?></td>
            </tr>
            <tr>
                <th>Total Positions</th>
                <td><?php echo $job_result->positions?></td>
            </tr>
            <tr>
                <th>City</th>
                <td>
                  <?php
                    $city = "";
                    if(count($job_cities) > 0 ) {
                      foreach ($job_cities as $c){
                        $city .=  $c['city_name'] .",";
                      }
                    }
                    echo rtrim($city,",");
                  ?>

                </td>
            </tr>
            <tr>
                <th>Country</th>
                <td><?php echo $job_result->country_name?></td>
            </tr>
            <tr>
                <th>Gender</th>
                <td><?php echo $job_result->gender?></td>
            </tr>
            <tr>
                <th>Age Requirement</th>
                <td><?php echo $job_result->age_title?></td>
            </tr>
            <tr>
                <th>Minimum Education Required</th>
                <td><?php echo $job_result->education_title?></td>
            </tr>
            <tr>
                <th>Required Qualification</th>
                <td> <?php $qualification = "";
                    if(count($jobs_qualifications) > 0 ) {
                      foreach ($jobs_qualifications as $q){
                        $qualification .=  $q['education_title'] .",";
                      }
                    }
                    echo rtrim($qualification,",");
                  ?></td>
            </tr>
            <tr>
                <th>Required Experience</th>
                <td><?php echo $job_result->experience_title?></td>
            </tr>
            <tr>
                <th>Salary Range</th>
                <td><?php echo $job_result->salaryrange_title?></td>
            </tr>
            <tr>
                <th>Date Posted</th>
                <td><?php echo short_date($job_result->posted_date)?></td>
            </tr>
            <tr>
                <th>Last Date to Apply</th>
                <td><?php echo short_date($job_result->last_date)?></td>
            </tr>
            <tr>
                <th>Where to Apply</th>
                <td><?php echo $job_result->where_to_apply?></td>
            </tr>
            <tr>
                <th>Source</th>
                <td><img src="<?php echo base_url()?>uploads/<?php echo $job_result->job_image?>" width="100"></td>
            </tr>
            
            
          </table>

           
      </div>
        <!-- Main Content End -->
       
        
        
    </div>
   </div>
   <!-- Container end -->
   
  <script type="text/javascript">
    $("#qualification_id").select2();
  </script>