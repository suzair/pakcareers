-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 22, 2014 at 10:45 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pakcareers`
--

-- --------------------------------------------------------

--
-- Table structure for table `age`
--

CREATE TABLE IF NOT EXISTS `age` (
  `age_id` int(11) NOT NULL AUTO_INCREMENT,
  `age_title` varchar(255) NOT NULL,
  `age_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`age_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `age`
--

INSERT INTO `age` (`age_id`, `age_title`, `age_status`) VALUES
(1, '20 - 30', 1),
(2, '20 - 31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE IF NOT EXISTS `career` (
  `career_id` int(11) NOT NULL AUTO_INCREMENT,
  `career_title` varchar(255) NOT NULL,
  `occupation_id` int(11) NOT NULL,
  `career_worksummary` text NOT NULL,
  `career_relatedenvironment` varchar(255) NOT NULL,
  `career_duties` text NOT NULL,
  `career_context` text NOT NULL,
  `career_knowledgeRequired` text NOT NULL,
  `career_skillsRequired` text NOT NULL,
  `career_abilitiesrequired` text NOT NULL,
  `occupationgroup_id` int(11) NOT NULL,
  `career_photo` varchar(255) NOT NULL,
  `career_video` varchar(255) NOT NULL,
  `career_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`career_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`career_id`, `career_title`, `occupation_id`, `career_worksummary`, `career_relatedenvironment`, `career_duties`, `career_context`, `career_knowledgeRequired`, `career_skillsRequired`, `career_abilitiesrequired`, `occupationgroup_id`, `career_photo`, `career_video`, `career_status`) VALUES
(1, '', 1, 'Assess and evaluate individuals'' problems through the use of case history, interview, and observation and provide individual or group counseling services to assist individuals in achieving more effective personal, social, educational, and vocational development and adjustment.', '', 'Collect information about individuals or clients, using interviews, case histories, observational techniques, and other assessment methods.\nDocument patient information including session notes, progress notes, recommendations, and treatment plans.\nCounsel individuals, groups, or families to help them understand problems, deal with crisis situations, define goals, and develop realistic action plans.\nDevelop therapeutic and treatment plans based on clients'' interests, abilities, and needs.\nSupervise interns, clinicians in training, and other counselors.\nAdvise clients on how they could be helped by counseling.\nAnalyze data such as interview notes, test results, and reference manuals to identify symptoms and to diagnose the nature of clients'' problems.\nConsult with other professionals, agencies, or universities to discuss therapies, treatments, counseling resources or techniques, and to share occupational information.\nEvaluate the results of counseling methods to determine the reliability and validity of treatments.\nRefer clients to specialists or to other institutions for noncounseling treatment of problems.', '', '', '', '                                          ', 15, '', '', 1),
(2, '', 2, 'Investigate processes of learning and teaching and develop psychological principles and techniques applicable to educational problems.', '', '•	Compile and interpret students'' test results, along with information from teachers and parents, to diagnose conditions, and to help assess eligibility for special services.\n•	Select, administer, and score psychological tests.\n•	Interpret test results and prepare psychological reports for teachers, administrators, and parents.\n•	Counsel children and families to help solve conflicts and problems in learning and adjustment.\n•	Provide consultation to parents, teachers, administrators, and others on topics such as learning styles and behavior modification techniques.\n•	Report any pertinent information to the proper authorities in cases of child endangerment, neglect, or abuse.\n•	Maintain student records, including special education reports, confidential records, records of services provided, and behavioral data.\n•	Assess an individual child''s needs, limitations, and potential, using observation, review of school records, and consultation with parents and school personnel.\n•	Collect and analyze data to evaluate the effectiveness of academic programs and other services, such as behavioral management systems.\n•	Promote an understanding of child development and its relationship to learning and behavior.\n', '', '', '', '                                          ', 15, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `careertrack`
--

CREATE TABLE IF NOT EXISTS `careertrack` (
  `careertrack_id` int(11) NOT NULL AUTO_INCREMENT,
  `careertrack_title` varchar(255) NOT NULL,
  `careertrack_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`careertrack_id`,`careertrack_title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=83 ;

--
-- Dumping data for table `careertrack`
--

INSERT INTO `careertrack` (`careertrack_id`, `careertrack_title`, `careertrack_status`) VALUES
(1, 'Food Products & Processing Systems', 1),
(2, 'Plant Systems', 1),
(3, 'Animal Systems', 1),
(4, 'Power, Structural & Technical Systems', 1),
(5, 'Natural Resources Systems', 1),
(6, 'Environmental Service Systems', 1),
(7, 'Agribusiness Systems', 1),
(8, 'Design/Pre-Construction', 1),
(9, 'Construction', 0),
(10, 'Maintenance/Operations', 1),
(11, 'A/V Technology & Film', 0),
(12, 'Printing Technology', 1),
(13, 'Visual Arts', 1),
(14, 'Performing Arts', 1),
(15, 'Journalism & Broadcasting', 1),
(16, 'Telecommunications', 1),
(17, 'General Management', 1),
(18, 'Business Information Management', 1),
(19, 'Human Resources Management', 1),
(20, 'Operations Management', 1),
(21, 'Administrative Support', 1),
(22, 'Administration & Administrative Support', 1),
(23, 'Professional Support Services', 1),
(24, 'Teaching/Training', 1),
(25, 'Securities & Investments', 1),
(26, 'Business Finance', 1),
(27, 'Accounting', 1),
(28, 'Insurance', 1),
(29, 'Banking Services', 1),
(30, 'Governance', 1),
(31, 'National Security', 1),
(32, 'Foreign Service', 1),
(33, 'Planning', 1),
(34, 'Revenue & Taxation', 1),
(35, 'Regulation', 1),
(36, 'Public Management & Administration', 1),
(37, 'Therapeutic Services', 1),
(38, 'Diagnostic Services', 1),
(39, 'Health Informatics', 1),
(40, 'Support Services', 1),
(41, 'Biotechnology Research & Development', 1),
(42, 'Restaurants & Food/Beverage Services', 1),
(43, 'Lodging', 1),
(44, 'Sales & Service', 1),
(45, 'Transportation Systems/Infrastructure Planning, Management & Regulation', 1),
(46, 'Warehousing & Distribution Center Operations', 1),
(47, 'Manufacturing Production Process Development', 1),
(48, 'Health, Safety & Environmental Assurance', 1),
(49, 'Early Childhood Development & Services', 1),
(50, 'Recreation, Amusements & Attractions', 1),
(51, 'Counseling & Mental Health Services', 1),
(52, 'Programming & Software Development', 1),
(53, 'Emergency & Fire Management Services', 1),
(54, 'Logistics Planning & Management Services', 1),
(55, 'Health, Safety & Environmental Management', 1),
(56, 'Facility & Mobile Equipment Maintenance', 1),
(57, 'Maintenance, Installation & Repair', 1),
(58, 'Web & Digital Communications', 1),
(59, 'Travel & Tourism', 1),
(60, 'Information Support & Services', 1),
(61, 'Family & Community Services', 1),
(62, 'Security & Protective Services', 1),
(63, 'Logistics & Inventory Control', 1),
(64, 'Marketing Communications', 1),
(65, 'Marketing Research', 1),
(66, 'Engineering & Technology', 1),
(67, 'Science & Math', 1),
(68, 'Law Enforcement Services', 1),
(69, 'Legal Services', 1),
(70, 'Production', 1),
(71, 'Quality Assurance', 1),
(72, 'Transportation Operations', 1),
(73, 'Marketing Management', 1),
(74, 'Correction Services', 1),
(75, 'Professional Sales', 1),
(76, 'Consumer Services', 1),
(77, 'Personal Care Services', 1),
(78, 'Network Systems', 1),
(79, 'Merchandising', 1),
(80, 'Business Financial Management and Accounting', 1),
(81, 'Marketing', 1),
(82, 'Business Analysis', 1);

-- --------------------------------------------------------

--
-- Table structure for table `career_careertrack`
--

CREATE TABLE IF NOT EXISTS `career_careertrack` (
  `ct_id` int(11) NOT NULL AUTO_INCREMENT,
  `career_id` int(11) NOT NULL,
  `careertrack_id` int(11) NOT NULL,
  PRIMARY KEY (`ct_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `career_careertrack`
--

INSERT INTO `career_careertrack` (`ct_id`, `career_id`, `careertrack_id`) VALUES
(1, 1, 51),
(2, 2, 37);

-- --------------------------------------------------------

--
-- Table structure for table `career_fieldofwork`
--

CREATE TABLE IF NOT EXISTS `career_fieldofwork` (
  `cf_id` int(11) NOT NULL AUTO_INCREMENT,
  `career_id` int(11) NOT NULL,
  `fieldofwork_id` int(11) NOT NULL,
  PRIMARY KEY (`cf_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `career_fieldofwork`
--

INSERT INTO `career_fieldofwork` (`cf_id`, `career_id`, `fieldofwork_id`) VALUES
(1, 1, 10),
(2, 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `career_occupations`
--

CREATE TABLE IF NOT EXISTS `career_occupations` (
  `career_occupation_id` int(11) NOT NULL AUTO_INCREMENT,
  `career_id` int(11) NOT NULL,
  `occupation_id` int(11) NOT NULL,
  PRIMARY KEY (`career_occupation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `career_studytrack`
--

CREATE TABLE IF NOT EXISTS `career_studytrack` (
  `cs_id` int(11) NOT NULL AUTO_INCREMENT,
  `career_id` int(11) NOT NULL,
  `studytrack_id` int(11) NOT NULL,
  PRIMARY KEY (`cs_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `career_studytrack`
--

INSERT INTO `career_studytrack` (`cs_id`, `career_id`, `studytrack_id`) VALUES
(1, 1, 47),
(2, 1, 37);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(255) NOT NULL,
  `city_status` tinyint(4) NOT NULL DEFAULT '1',
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_status`, `country_id`) VALUES
(1, 'Lahore', 1, 1),
(2, 'Multan', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(255) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `country_logo` varchar(255) NOT NULL,
  `country_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`, `country_code`, `country_logo`, `country_status`) VALUES
(1, 'Pakistan', '09', '', 1),
(2, 'India', 'IN', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(255) NOT NULL,
  `course_levelofeducation` varchar(255) NOT NULL,
  `course_degreeprogram` varchar(255) NOT NULL,
  `course_duration` varchar(255) NOT NULL,
  `course_institution` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `course_fieldofeducation` varchar(255) NOT NULL,
  `studytrack_id` int(11) NOT NULL,
  `course_fee` varchar(255) NOT NULL,
  `course_admissiondeadline` datetime NOT NULL,
  `course_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_id`, `course_name`, `course_levelofeducation`, `course_degreeprogram`, `course_duration`, `course_institution`, `city_id`, `course_fieldofeducation`, `studytrack_id`, `course_fee`, `course_admissiondeadline`, `course_status`) VALUES
(1, 'p', 'p', 'p', 'p', 'p', 1, 'p', 0, 'p', '1970-01-01 12:00:00', 1),
(2, 'fdsfdsf', '', '', '', '', 0, '', 0, '', '0000-00-00 00:00:00', 1),
(3, 'Marcia Chaney', 'Laborum Sunt ducimus omnis aliquid quis', 'Voluptas voluptatibus eos excepturi ad porro nostrum elit repudiandae earum', 'Ut non est quasi assumenda facere nisi ut', 'Facere quia reiciendis ut cum ad culpa incididunt in eum qui consectetur velit et', 1, 'Neque rerum sit commodi praesentium esse sint ullamco veniam sed', 45, 'Est corporis placeat id mollit possimus occaecat qui', '1988-05-14 12:00:00', 1),
(4, 'Jana Ballard', 'Quia nesciunt et sit eum occaecat non accusantium cupidatat cum alias non placeat', 'Ipsam sint ex eligendi laborum', 'Odio voluptatem Ut ab quis rerum consequatur voluptas natus', 'Quasi consequatur Explicabo Ipsum ipsum veniam irure ut cupiditate sequi impedit illum quae id sed qui adipisci', 1, 'Eos sint labore et est culpa aut aperiam eligendi ipsum veniam reprehenderit', 37, 'Voluptatem reiciendis rem non ut velit ratione soluta ullam et minima', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE IF NOT EXISTS `education` (
  `education_id` int(11) NOT NULL AUTO_INCREMENT,
  `education_title` varchar(255) NOT NULL,
  `education_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`education_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`education_id`, `education_title`, `education_status`) VALUES
(1, 'Primary (up to 5th Grade)', 1),
(2, 'Middle (6th to 8th Grade)', 1),
(3, 'Matriculation/SSC/Secondary (9th and 10th Grade)', 1),
(4, 'Intermediate/HSC (11th and 12th Grade)', 1),
(5, 'Graduation/Bachelors (14 Years of Education)', 1),
(6, 'Masters (16 Years of Education)', 1),
(7, 'MS/MPhil (18 Years of Education)', 1),
(8, 'PhD/Doctorate Degree', 1),
(9, 'Professional Qualification in Accountancy', 1),
(10, 'Major Training of Medical Field ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE IF NOT EXISTS `experience` (
  `experience_id` int(11) NOT NULL AUTO_INCREMENT,
  `experience_title` varchar(255) NOT NULL,
  `experience_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`experience_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`experience_id`, `experience_title`, `experience_status`) VALUES
(1, '10 Years', 1),
(2, '30 Years', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fieldofwork`
--

CREATE TABLE IF NOT EXISTS `fieldofwork` (
  `fieldofwork_id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldofwork_title` varchar(255) NOT NULL,
  `fieldofwork_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`fieldofwork_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `fieldofwork`
--

INSERT INTO `fieldofwork` (`fieldofwork_id`, `fieldofwork_title`, `fieldofwork_status`) VALUES
(1, 'Agriculture, Food & Natural Resources', 1),
(2, 'Architecture & Construction', 1),
(3, 'Arts, Audio/Video Technology & Communications', 1),
(4, 'Business Management & Administration', 1),
(5, 'Education & Training', 1),
(6, 'Finance', 1),
(7, 'Government & Public Administration', 1),
(8, 'Health Science', 1),
(9, 'Hospitality & Tourism', 1),
(10, 'Human Services', 1),
(11, 'Information Technology', 1),
(12, 'Law, Public Safety, Corrections & Security', 1),
(13, 'Manufacturing', 1),
(14, 'Marketing', 1),
(15, 'Science, Technology, Engineering & Mathematics', 1),
(16, 'Transportation, Distribution & Logistics', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jobgroup`
--

CREATE TABLE IF NOT EXISTS `jobgroup` (
  `jobgroup_id` int(11) NOT NULL AUTO_INCREMENT,
  `jobgroup_title` varchar(255) NOT NULL,
  `jobgroup_status` tinyint(4) NOT NULL DEFAULT '1',
  `occupation_id` int(11) NOT NULL,
  PRIMARY KEY (`jobgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_title` varchar(255) NOT NULL,
  `jobsector_id` int(11) NOT NULL,
  `positions` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `agegroup_id` int(11) NOT NULL,
  `education_id` int(11) NOT NULL,
  `qualification` int(11) NOT NULL,
  `experience_id` int(11) NOT NULL,
  `salaryrange_id` int(11) NOT NULL,
  `jobtype_id` int(11) NOT NULL,
  `posted_date` datetime NOT NULL,
  `last_date` datetime NOT NULL,
  `organization_id` int(11) NOT NULL,
  `where_to_apply` varchar(255) NOT NULL,
  `jobsource_id` int(11) NOT NULL,
  `job_image` varchar(255) NOT NULL,
  `occupation_id` int(11) NOT NULL,
  `jobgroup_id` int(11) NOT NULL,
  `job_view` int(11) NOT NULL,
  `jobs_status` tinyint(4) NOT NULL DEFAULT '1',
  `occupationgroup_id` int(11) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`job_id`, `job_title`, `jobsector_id`, `positions`, `city_id`, `country_id`, `gender`, `agegroup_id`, `education_id`, `qualification`, `experience_id`, `salaryrange_id`, `jobtype_id`, `posted_date`, `last_date`, `organization_id`, `where_to_apply`, `jobsource_id`, `job_image`, `occupation_id`, `jobgroup_id`, `job_view`, `jobs_status`, `occupationgroup_id`) VALUES
(1, 'Minima labore irure consectetur Nam dolor dolores ea aliquid excepteur rem voluptatibus officiis consectetur illum sint quis et', 1, 0, 1, 1, 'Gender Does not matter', 2, 2, 0, 2, 7, 5, '2007-10-04 12:00:00', '1988-12-10 12:00:00', 1, 'Rem aut quo magna aliquid suscipit quaerat ducimus sequi reprehenderit et nesciunt cillum eiusmod consequat', 2, 'Desert1.jpg', 56, 0, 0, 1, 16),
(2, 'Aut nihil dolorem tempore ut ea assumenda id quisquam', 2, 0, 1, 2, 'Females encouraged to apply (Males can also apply)', 1, 10, 0, 1, 6, 4, '2009-05-24 12:00:00', '1970-06-15 12:00:00', 2, 'Et eaque id molestias animi iusto vero hic ea qui velit', 2, 'Desert2.jpg', 9, 0, 0, 1, 10),
(3, 'In recusandae Veniam expedita voluptatem Ut omnis', 2, 0, 1, 2, 'Only for Males', 2, 9, 0, 1, 1, 3, '1971-01-09 12:00:00', '1977-09-04 12:00:00', 1, 'Itaque Nam dolorem in quia quas officiis duis exercitation neque voluptas beatae omnis enim cupiditate consequatur quia numquam', 1, '1622020_798193493528788_1432183865_n.jpg', 2, 0, 0, 1, 22);

-- --------------------------------------------------------

--
-- Table structure for table `jobsector`
--

CREATE TABLE IF NOT EXISTS `jobsector` (
  `jobsector_id` int(11) NOT NULL AUTO_INCREMENT,
  `jobsector_title` varchar(255) NOT NULL,
  `jobsector_logo` varchar(255) NOT NULL,
  `jobsector_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`jobsector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jobsector`
--

INSERT INTO `jobsector` (`jobsector_id`, `jobsector_title`, `jobsector_logo`, `jobsector_status`) VALUES
(1, 'Private Job', '', 1),
(2, 'Government Job', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jobsource`
--

CREATE TABLE IF NOT EXISTS `jobsource` (
  `jobsource_id` int(11) NOT NULL AUTO_INCREMENT,
  `jobsource_title` varchar(255) NOT NULL,
  `jobsource_logo` varchar(255) NOT NULL,
  `jobsource_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`jobsource_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jobsource`
--

INSERT INTO `jobsource` (`jobsource_id`, `jobsource_title`, `jobsource_logo`, `jobsource_status`) VALUES
(1, 'Jang', '', 1),
(2, 'Nawa-e-Qaqt', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jobs_careertrack`
--

CREATE TABLE IF NOT EXISTS `jobs_careertrack` (
  `jc_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `careertrack_id` int(11) NOT NULL,
  PRIMARY KEY (`jc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jobs_careertrack`
--

INSERT INTO `jobs_careertrack` (`jc_id`, `job_id`, `careertrack_id`) VALUES
(1, 2, 21);

-- --------------------------------------------------------

--
-- Table structure for table `jobs_cities`
--

CREATE TABLE IF NOT EXISTS `jobs_cities` (
  `jobs_cities_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`jobs_cities_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobs_fieldofwork`
--

CREATE TABLE IF NOT EXISTS `jobs_fieldofwork` (
  `jf_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `fieldofwork_id` int(11) NOT NULL,
  PRIMARY KEY (`jf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobs_qualification`
--

CREATE TABLE IF NOT EXISTS `jobs_qualification` (
  `job_qualification_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `education_id` int(11) NOT NULL,
  PRIMARY KEY (`job_qualification_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobtype`
--

CREATE TABLE IF NOT EXISTS `jobtype` (
  `jobtype_id` int(11) NOT NULL AUTO_INCREMENT,
  `jobtype_title` varchar(255) NOT NULL,
  `jobtype_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`jobtype_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `jobtype`
--

INSERT INTO `jobtype` (`jobtype_id`, `jobtype_title`, `jobtype_status`) VALUES
(1, 'Not mentioned in the ad', 1),
(2, 'Full Time (Contract)', 1),
(3, 'Part Time (Contract)', 1),
(4, 'Full Time (Permanent)', 1),
(5, 'Part Time (Permanent)', 1),
(6, 'Internship', 1);

-- --------------------------------------------------------

--
-- Table structure for table `management`
--

CREATE TABLE IF NOT EXISTS `management` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rank` varchar(200) NOT NULL,
  `last_login` datetime NOT NULL,
  `last_ip` varchar(255) NOT NULL,
  `user_status` int(11) NOT NULL DEFAULT '1',
  `useremail` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `management`
--

INSERT INTO `management` (`user_id`, `username`, `name`, `password`, `rank`, `last_login`, `last_ip`, `user_status`, `useremail`) VALUES
(1, 'admin', 'Admin\n', '21232f297a57a5a743894a0e4a801fc3', '1', '2013-12-08 00:00:00', '111111', 1, 'syeduzairahmad@live.com'),
(4, 'uzairshah', 'Uzair Shah', 'uzairshah', 'super_admin', '0000-00-00 00:00:00', '', 0, ''),
(6, 'admin', 'Admin', '21232f297a57a5a743894a0e4a801fc3', 'super_admin', '0000-00-00 00:00:00', '', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `occupation`
--

CREATE TABLE IF NOT EXISTS `occupation` (
  `occupation_id` int(11) NOT NULL AUTO_INCREMENT,
  `occupation_title` varchar(255) NOT NULL,
  `occupation_othertitles` text NOT NULL,
  `occupationgroup_id` int(11) NOT NULL,
  `fieldofwork_id` int(11) NOT NULL,
  `careertrack_id` int(11) NOT NULL,
  `occupation_status` tinyint(4) NOT NULL DEFAULT '1',
  `occupation_video` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`occupation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `occupation`
--

INSERT INTO `occupation` (`occupation_id`, `occupation_title`, `occupation_othertitles`, `occupationgroup_id`, `fieldofwork_id`, `careertrack_id`, `occupation_status`, `occupation_video`) VALUES
(1, 'Counseling Psychologists', 'Applied Behavior Science Specialist (ABSS), Chemical Dependency Therapist, Counseling Psychologist, Counseling Services Director, Counselor, Licensed Professional Counselor (LPC), Psychologist, Psychotherapist, Senior Staff Psychologist, Staff Psychologist ', 15, 10, 51, 1, ''),
(2, 'Pilots, Ship', 'Pilot, Docking Pilot, Ship Pilot, River Pilot, Towboat Pilot, Harbor Pilot, State Pilot, Bar Pilot, Marine Pilot, Relief Docking Master ', 23, 16, 72, 1, ''),
(3, 'Industrial-Organizational Psychologists', 'Assessment Services Manager, Consultant, Consulting Psychologist, Industrial Psychologist, Industrial/Organizational Psychologist (I/O Psychologist), Management Consultant, Organizational Consultant, Organizational Development Consultant, Organizational Psychologist, Research Scientist   ', 15, 8, 37, 1, ''),
(4, 'Animal Scientists', 'Animal Nutritionist, Animal Scientist, Beef Cattle Specialist, Research and Development Director, Animal Management Systems Specialist, Animal Nutrition Consultant, Animal Science Section Leader, Beef Cattle Nutritionist, Beef Technical Services Manager, Dairy Consultant ', 15, 1, 3, 1, ''),
(5, 'Architectural Drafters  ', 'Intern Architect, Drafter, Architect, Draftsman, Architectural Designer, Architectural Drafter, Architectural Intern, Project Manager, Architectural Draftsman, CAD Technician (Computer-Aided Design Technician) ', 2, 2, 8, 1, ''),
(6, 'Agricultural Engineers', 'Professor, Engineer, Project Engineer, Agricultural Engineer, Agricultural Safety and Health Program Director, Assistant Professor, Research Agricultural Engineer, Research Leader, Agricultural Systems Specialist, Conservation Engineer ', 2, 15, 66, 1, ''),
(7, 'Archeologists', 'Archaeologist, Associate Director, Curator, Director of Research Center, Principal Archaeologist, Project Director, Research Archaeologist  ', 15, 15, 67, 1, ''),
(8, 'Archivists', 'Archivist, Registrar, Archives Director, Manuscripts Curator, Collections Manager, Museum Archivist, Records Manager, University Archivist, Archival Records Clerk, Collections Director  ', 8, 3, 11, 1, ''),
(9, 'Audiologists  ', 'Audiologist, Clinical Audiologist, Audiology Doctor (AUD), Dispensing Audiologist, Audiology Director, Educational Audiologist, Pediatric Audiologist, Certificate of Clinical Competence in Audiology Licensed Audiologist (CCC-A Licensed Audiologist), Clinical Director, Doctor of Audiology  ', 11, 8, 37, 1, ''),
(10, 'Auditors', 'Auditor, Internal Auditor, Auditor-in-Charge, Assurance Manager, Audit Manager, Internal Audit Director, Assurance Senior, Audit Partner, Deputy for Audit, Financial Auditor ', 6, 4, 80, 1, ''),
(11, 'Barbers', 'Barber, Stylist, Master Barber, Barber Shop Operator  ', 19, 10, 77, 1, ''),
(12, 'Biologists', 'Scientist, Biologist, Environmental Analyst, Research Scientist, Environmental Specialist, Fisheries Biologist, Research Biologist, Aquatic Scientist, Assistant Scientist, Marine Biologist  ', 19, 8, 41, 1, ''),
(13, 'Chemical Engineers  ', 'Process Engineer, Chemical Engineer, Engineer, Scientist, Project Engineer, Development Engineer, Engineering Scientist, Process Control Engineer, Process Development Engineer, Refinery Process Engineer  ', 2, 15, 66, 1, ''),
(14, 'Chemists', 'Air Quality Chemist, Analytical Chemist, Chemical Laboratory Scientist, Chemist, Forensic Chemist, Forensic Scientist, Quality Control Chemist (QC Chemist), Research Chemist, Scientist, Senior Chemist  ', 15, 15, 67, 1, ''),
(15, 'Chief Executives  ', 'Chief Executive Officer (CEO), President, Chief Financial Officer (CFO), Vice President, Chief Operating Officer (COO), Executive Director, Executive Vice President (EVP), Finance Vice President, General Manager, Operations Vice President  ', 16, 4, 80, 1, ''),
(16, 'Civil Engineers   ', 'Civil Engineer, Engineer, Project Engineer, Project Manager, Structural Engineer, City Engineer, Civil Engineering Manager, Design Engineer, Railroad Design Consultant, Research Hydraulic Engineer  ', 2, 2, 8, 1, ''),
(17, 'Clinical Psychologists', 'Child Psychologist, Clinical Director, Clinical Psychologist, Clinical Therapist, Forensic Psychologist, Licensed Clinical Psychologist, Licensed Psychologist, Licensed Psychologist Manager, Pediatric Psychologist, Psychologist  ', 15, 10, 51, 1, ''),
(18, 'Computer Programmers      ', 'Programmer Analyst, Programmer, Analyst Programmer, Computer Programmer, Software Developer, Applications Developer, Computer Programmer Analyst, Internet Programmer, Java Developer, Web Programmer ', 1, 3, 11, 1, ''),
(19, 'Curators', 'Curator, Museum Curator, Collections Curator, Associate Curator, Collections Manager, Curator of Collections, Curator of Education, Exhibitions Curator, Exhibits Curator, Gallery Director  ', 8, 3, 11, 1, ''),
(20, 'Editors', 'Editor, Managing Editor, Newspaper Copy Editor, Sports Editor, Copy Editor, News Editor, Features Editor, Assignment Editor, City Editor, Copy Desk Chief  ', 3, 8, 39, 1, ''),
(21, 'Electricians    ', 'Chief Electrician; Control Electrician; Electrician; Industrial Electrician; Inside Wireman; Journeyman Electrician; Journeyman Wireman; Maintenance Electrician; Mechanical Trades Specialist, Electrician; Qualified Craft Worker, Electrician (QCW, Electrician)  ', 7, 2, 9, 1, ''),
(22, 'Cooks, Fast Food  ', 'Cook, Pizza Cook, Grill Cook, Crew Member, Line Cook, Crew Person, Crew Trainer, Fry Cook, Pizza Maker, Prep Cook  ', 10, 9, 42, 1, ''),
(23, 'Fashion Designers', 'Designer, Fashion Designer, Design Director, Costume Designer, Product Developer, Apparel Fashion Designer, Clothing Designer, Dance Costume Designer, Historic Clothing and Costume Maker, Latex Fashions Designer  ', 3, 10, 76, 1, ''),
(24, 'Genetic Counselors', 'Genetic Counselor, Hereditary Cancer Program Coordinator, Prenatal and Pediatric Genetic Counselor, Reproductive Genetic Counseling Coordinator  ', 11, 8, 37, 1, ''),
(25, 'Geneticists  ', 'Professor, Assistant Professor, Associate Professor, Research Scientist, Associate Professor of Genetics, Clinical Cytogenetics Director, Clinical Molecular Genetics Laboratory Director, Medical Genetics Director  ', 15, 15, 67, 1, ''),
(26, 'Graphic Designers  ', 'Graphic Designer, Graphic Artist, Designer, Creative Director, Artist, Design Director, Composing Room Supervisor, Creative Manager, Desktop Publisher, Graphic Designer/Production  ', 3, 1, 7, 1, ''),
(27, 'Neurologists  ', 'Adult Neurologist; Adult and Pediatric Neurologist; Attending Physician; Director of Adult Epilepsy; Director, Inpatient Headache Program; General Neurologist; Medical Director of MS Treatment and Research Center; Neurologist; Neurologist and Director of Medical Research; Pediatric Neurologist  ', 11, 8, 37, 1, ''),
(28, 'Naturopathic Physicians  ', 'Chief Medical Officer; Chief Medical Officer, Naturopathic Endocrinologist and Oncologist; Clinical Director; Doctor (Dr); Doctor of Naturopathic Medicine; Medical Director; Naturopathic Doctor; Naturopathic Oncology Provider; Naturopathic Physician; Physician  ', 11, 8, 37, 1, ''),
(29, 'Occupational Therapists  ', 'Occupational Therapist (OT), Registered Occupational Therapist, Staff Therapist, Assistive Technology Trainer, Industrial Rehabilitation Consultant  ', 11, 8, 39, 1, ''),
(30, 'Optometrists  ', 'Optometrist, Doctor of Optometry (OD), Doctor ', 11, 8, 37, 1, ''),
(31, 'Pathologists  ', 'Pathologist, Pathology Laboratory Director, Attending Pathologist, Anatomic Pathologist, Associate Pathologist, Cytopathologist, Dermatopathologist, Forensic Pathologist, Oral Pathologist  ', 11, 8, 37, 1, ''),
(32, 'Pharmacists  ', 'Clinical Pharmacist; Hospital Pharmacist; Outpatient Pharmacy Manager; Pharmacist; Pharmacist in Charge (PIC); Pharmacist in Charge, Owner (PIC, Owner); Pharmacy Informaticist; Registered Pharmacist; Staff Pharmacist; Staff Pharmacist, Hospital  ', 11, 8, 41, 1, ''),
(33, 'Philosophy and Religion Teachers, Postsecondary', 'Professor, Philosophy Professor, Religion Professor, Assistant Professor, Instructor, Philosophy Instructor, Professor of Philosophy, Religious Studies Professor, Humanities Professor, Assistant Professor of Philosophy  ', 8, 10, 51, 1, ''),
(34, 'Physical Therapists ', 'Chief Physical Therapist; Home Care Physical Therapist; Outpatient Physical Therapist; Pediatric Physical Therapist; Per Diem Physical Therapist; Physical Therapist (PT); Physical Therapist, Director of Rehabilitation; Registered Physical Therapist (RPT); Rehabilitation Services Director; Staff Physical Therapist (Staff PT)  ', 11, 8, 39, 1, ''),
(35, 'Physicists', 'Health Physicist, Scientist, Research Scientist, Physicist, Research Consultant, Research Physicist, Biophysics Scientist  ', 15, 8, 39, 1, ''),
(36, 'Plumbers', 'Commercial Plumber; Drain Cleaner, Plumber; Drain Technician; Journeyman Plumber; Master Plumber; Plumber; Plumber Gasfitter; Plumbing and Heating Mechanic; Residential Plumber; Service Plumber  ', 7, 2, 9, 1, ''),
(37, 'Podiatrists', 'Podiatrist, Podiatric Physician, Doctor of Podiatric Medicine (DPM), Podiatric Surgeon, Doctor Podiatric Medicine (DPM), Foot and Ankle Surgeon, Physician, Doctor, Practitioner  ', 11, 8, 37, 1, ''),
(38, 'Police Detectives', 'Detective, Fugitive Detective, Investigator, Police Detective, Narcotics Detective, Fugitive Investigator, Narcotics Investigator, Detective Sergeant, Detective Supervisor, Sex Crimes Detective  ', 21, 12, 68, 1, ''),
(39, 'Producers', 'Producer, News Producer, Television News Producer, Promotions Producer, Television Producer (TV Producer), Animation Producer, Executive Producer, Newscast Producer, Radio Producer, Associate Producer   ', 3, 3, 15, 1, ''),
(40, 'Orthotists and Prosthetists', 'American Board Certified Orthotist (ABC Orthotist), Certified Orthotist (CO), Certified Prosthetist (CP), Certified Prosthetist/Orthotist (CPO), Licensed Prosthetist/Orthotist (LPO), Orthotic/Prosthetic Practitioner, Orthotist, Orthotist/Prosthetist, Pedorthist, Prosthetist ', 11, 8, 39, 1, ''),
(41, 'Prosthodontists', 'Doctor of Dental Science, Prosthodontist; Doctor of Dental Surgery (DDS); Maxillofacial Prosthodontist; Prosthetic Dentist; Prosthodontist; Prosthodontist, Assistant Clinical Professor; Prosthodontist/Educator; Prosthodontist/Owner; Prosthodontist/Restorative/Reconstructive Dentist; Removable Prosthodontist  ', 11, 8, 37, 1, ''),
(42, 'Psychiatrists', 'Psychiatrist, Staff Psychiatrist, Child Psychiatrist, Consulting Psychiatrist, Prison Psychiatrist  ', 11, 8, 37, 1, ''),
(43, 'Radiologists  ', 'Attending Radiologist; Diagnostic Radiologist; Interventional Neuroradiologist; Interventional Radiologist; Neuroradiologist; Physician; Radiologist; Radiologist, Chief of Breast Imaging; Radiology Resident; Staff Radiologist  ', 11, 8, 38, 1, ''),
(44, 'Sales Managers  ', 'Sales Manager, Vice President of Sales, Director of Sales, District Sales Manager, Regional Sales Manager, Sales Supervisor, General Manager, Sales and Marketing Vice President, Sales Representative, Store Manager  ', 16, 10, 76, 1, ''),
(45, 'School Psychologists', 'School Psychologist, Psychologist, Consulting Psychologist, Early Intervention School Psychologist, Bilingual School Psychologist, Child Study Team Director, School Psychometrist  ', 15, 10, 51, 1, ''),
(46, 'Skincare Specialists', 'Aesthetician, Clinical Esthetician, Esthetician, Facialist, Lead Esthetician, Medical Esthetician, Skin Care Specialist, Skin Care Technician, Skin Care Therapist, Spa Technician  ', 19, 10, 77, 1, ''),
(47, 'Sociologists', 'Research Scientist, Research Associate, Social Scientist, Behavioral Scientist, Foundation Program Director, International Health Director (Health Science Administration), Policy Analyst, Research and Evaluation Manager, Research Center Director, Research Coordinator  ', 15, 2, 9, 1, ''),
(48, 'Software Developers, Applications  ', 'Software Engineer, Application Integration Engineer, Programmer Analyst, Software Development Engineer, Computer Consultant, Software Architect, Software Developer, Technical Consultant, Applications Developer, Business Systems Analyst ', 1, 8, 39, 1, ''),
(49, 'Speech-Language Pathologists', 'Bilingual Speech-Language Pathologist, Communication Specialist, Educational Speech-Language Clinician, Speech Pathologist, Speech Therapist, Speech and Language Clinician, Speech and Language Specialist, Speech-Language Pathologist (SLP), Speech/Language Therapist, Teacher of the Speech and Hearing Handicapped', 11, 8, 37, 1, ''),
(50, 'Sports Medicine Physicians ', 'Athletic Team Physician; Director of Athletic Medicine, Head Team Physician; Director of Sports Medicine; Family Medicine/Sports Medicine Specialist/Team Physician; Head Orthopedic Team Physician; Nonsurgical Primary Care Sports Medicine Physician; Orthopaedic Surgeon; Physician; Sports Medicine Physician; Team Physician  ', 11, 8, 37, 1, ''),
(51, 'Statisticians', 'Assistant Division Chief for Statistical Program Management, Clinical Statistics Manager, Human Resource Statistician, Private Statistical/Psychometric Consultant, Program Research Specialist, Senior Statistician, Statistical Analyst, Statistician, Statistician (Demographer), Trend Investigator  ', 1, 4, 80, 1, ''),
(52, 'Surgeons', 'General Surgeon, Surgeon, Physician, Orthopedic Surgeon, Medical Doctor (MD), Plastic Surgeon, Vascular Surgeon, Cardiovascular Surgeon, Orthopaedic Surgeon, Hand Surgeon  ', 11, 8, 37, 1, ''),
(53, 'Tailors, Dressmakers, and Custom Sewers', 'Seamstress, Tailor, Alterations Sewer, Dressmaker, Custom Dressmaker, Designer, Prototype Technician, Alterations Expert, Couture Alterations Dressmaker, Couturiere  ', 20, 10, 77, 1, ''),
(54, 'Teacher Assistants  ', 'Teacher Assistant, Paraprofessional, Instructional Assistant, Special Education Teaching Assistant, Paraeducator, Teacher Aide, Special Education Paraprofessional, Teaching Assistant, Educational Technician, Instructional Aide ', 8, 5, 24, 1, ''),
(55, 'Technical Writers', 'Documentation Designer, Documentation Specialist, Engineering Writer, Expert Medical Writer, Information Developer, Narrative Writer, Requirements Analyst, Senior Technical Writer, Technical Communicator, Technical Writer  ', 3, 10, 76, 1, ''),
(56, 'Interpreters and Translators ', 'Court Interpreter, Deaf Interpreter, Educational Interpreter, Interpreter, Medical Interpreter, Paraprofessional Interpreter, Sign Language Interpreter, Spanish Interpreter, Technical Translator, Translator ', 3, 10, 76, 1, ''),
(57, 'Transportation Engineers', 'Project Manager, Transportation Engineer, Traffic Operations Engineer ', 2, 15, 66, 1, ''),
(58, 'Urologists  ', 'Acute Care Physician, Attending Physician, Attending Urologist, Chief of Pediatric Urology, MD Urologist, Medical Doctor, Physician, Urologic Surgeon, Urologist, Urologist MD  ', 11, 8, 37, 1, ''),
(59, 'Veterinarians  ', 'Associate Veterinarian, Doctor of Veterinary Medicine (DVM), Emergency Veterinarian, Equine Vet (Equine Veterinarian), Mixed Animal Veterinarian, Small Animal Veterinarian, Staff Veterinarian, Veterinarian (VET), Veterinary Medicine Doctor (DVM), Veterinary Surgeon  ', 11, 1, 3, 1, ''),
(60, 'Accountants', 'Accountant, Certified Public Accountant (CPA), Staff Accountant, Accounting Manager, Cost Accountant, General Accountant, Accounting Officer, Business Analyst, Accounting Supervisor, Financial Reporting Accountant  ', 6, 4, 80, 1, ''),
(61, 'Actors', 'Actor, Actress, Comedian, Comic, Community Theater Actor, Narrator, Voice-Over Artist, Stand Up Comedian, Theatre Ensemble Member, Understudy  ', 3, 3, 14, 1, ''),
(62, 'Airline Pilots, Copilots, and Flight Engineers', 'Airline Captain, Airline Pilot, Airline Pilot (Captain), Airline Transport Pilot, Captain, Check Airman, Co-Pilot, Commuter Pilot, First Officer, Pilot  ', 23, 2, 9, 1, ''),
(63, 'Animal Trainers', 'Trainer, Dog Trainer, Horse Trainer, Guide Dog Instructor, Guide Dog Trainer, Agility Instructor, Cutting Horse Trainer, Dog and Cat Behavior Specialist, Guide Dog Mobility Instructor, Licensed Guide Dog Instructor ', 19, 1, 3, 1, ''),
(64, 'Art Directors', 'Art Director; Art Supervisor; Creative Director; Creative Guru; Design Director; Designer; Director of Creative Services, Consumer Products; Group Art Supervisor; Production Manager; Senior Art Director   ', 3, 4, 81, 1, ''),
(65, 'Biochemical Engineers    ', 'Engineering Director, Process Engineer  ', 2, 15, 66, 1, ''),
(66, 'Budget Analysts', 'Budget Analyst, Budget Officer, Budget and Policy Analyst, Chief Financial Officer (CFO), Cost Accountant, Staff Analyst, Accounting Supervisor, Budget Coordinator   ', 6, 4, 82, 1, ''),
(67, 'Compliance Managers    ', 'Business Practices Supervisor; Compliance Coordinator; Compliance Engineer-Products; Compliance Manager; Compliance Officer; Compliance Review Officer; Corporate Operations Compliance Manager; Director of Compliance; Director, Global Ethics & Compliance and Assistant General Counsel; Internal Review and Audit Compliance ', 16, 4, 17, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `occupationgroup`
--

CREATE TABLE IF NOT EXISTS `occupationgroup` (
  `occupationgroup_id` int(11) NOT NULL AUTO_INCREMENT,
  `occupationgroup_title` varchar(255) NOT NULL,
  `occupationgroup_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`occupationgroup_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `occupationgroup`
--

INSERT INTO `occupationgroup` (`occupationgroup_id`, `occupationgroup_title`, `occupationgroup_status`) VALUES
(1, 'Computer and Mathematical', 1),
(2, 'Architecture and Engineering', 1),
(3, 'Arts, Design, Entertainment, Sports, and Media', 1),
(4, 'Building and Grounds Cleaning and Maintenance', 1),
(5, 'Community and Social Service', 1),
(6, 'Business and Financial Operations', 1),
(7, 'Construction and Extraction', 1),
(8, 'Education, Training, and Library', 1),
(9, 'Farming, Fishing, and Forestry', 1),
(10, 'Food Preparation and Serving Related', 1),
(11, 'Healthcare Practitioners and Technical', 1),
(12, 'Healthcare Support', 1),
(13, 'Installation, Maintenance, and Repair', 1),
(14, 'Legal', 1),
(15, 'Life, Physical, and Social Science', 1),
(16, 'Management', 1),
(17, 'Military Specific', 1),
(18, 'Office and Administrative Support', 1),
(19, 'Personal Care and Service', 1),
(20, 'Production', 1),
(21, 'Protective Service', 1),
(22, 'Sales and Related', 1),
(23, 'Transportation and Material Moving', 1);

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE IF NOT EXISTS `organization` (
  `organization_id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_name` varchar(255) NOT NULL,
  `organization_logo` varchar(255) NOT NULL,
  `organization_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`organization_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`organization_id`, `organization_name`, `organization_logo`, `organization_status`) VALUES
(1, 'Test', '', 1),
(2, 'ABC', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `salaryrange`
--

CREATE TABLE IF NOT EXISTS `salaryrange` (
  `salaryrange_id` int(11) NOT NULL AUTO_INCREMENT,
  `salaryrange_title` varchar(255) NOT NULL,
  `salaryrange_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`salaryrange_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `salaryrange`
--

INSERT INTO `salaryrange` (`salaryrange_id`, `salaryrange_title`, `salaryrange_status`) VALUES
(1, '0 to 5,000 (PKR)', 1),
(2, '05,000 to 10,000 (PKR)', 1),
(3, '10,000 to 20,000 (PKR)', 1),
(4, '20,000 to 30,000 (PKR)', 1),
(5, '30,000 to 40,000 (PKR)', 1),
(6, '40,000 to 50,000 (PKR)', 1),
(7, '50,000 to 60,000 (PKR)', 1),
(8, '60,000 to 70,000 (PKR)', 1),
(9, '70,000 to 80,000 (PKR)', 1),
(10, '80,000 plus (PKR)', 1),
(11, 'Not Mentioned in the Ad', 1);

-- --------------------------------------------------------

--
-- Table structure for table `studytrack`
--

CREATE TABLE IF NOT EXISTS `studytrack` (
  `studytrack_id` int(11) NOT NULL AUTO_INCREMENT,
  `studytrack_title` varchar(255) NOT NULL,
  `studytrack_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`studytrack_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

--
-- Dumping data for table `studytrack`
--

INSERT INTO `studytrack` (`studytrack_id`, `studytrack_title`, `studytrack_status`) VALUES
(1, 'Food Products & Processing Systems', 1),
(2, 'Plant Systems', 1),
(3, 'Animal Systems', 1),
(4, 'Power, Structural & Technical Systems', 1),
(5, 'Natural Resources Systems', 1),
(6, 'Environmental Service Systems', 1),
(7, 'Agribusiness Systems', 1),
(8, 'Design/Pre-Construction', 1),
(9, 'Construction', 1),
(10, 'Maintenance/Operations', 1),
(11, 'A/V Technology & Film', 1),
(12, 'Printing Technology', 1),
(13, 'Visual Arts', 1),
(14, 'Performing Arts', 1),
(15, 'Journalism & Broadcasting', 1),
(16, 'Telecommunications', 1),
(17, 'General Management', 1),
(18, 'Business Information Management', 1),
(19, 'Human Resources Management', 1),
(20, 'Operations Management', 1),
(21, 'Administrative Support', 1),
(22, 'Administration & Administrative Support', 1),
(23, 'Professional Support Services', 1),
(24, 'Teaching/Training', 1),
(25, 'Securities & Investments', 1),
(26, 'Business Finance', 1),
(27, 'Accounting', 1),
(28, 'Insurance', 1),
(29, 'Banking Services', 1),
(30, 'Governance', 1),
(31, 'National Security', 1),
(32, 'Foreign Service', 1),
(33, 'Planning', 1),
(34, 'Revenue & Taxation', 1),
(35, 'Regulation', 1),
(36, 'Public Management & Administration', 1),
(37, 'Therapeutic Services', 1),
(38, 'Diagnostic Services', 1),
(39, 'Health Informatics', 1),
(40, 'Support Services', 1),
(41, 'Biotechnology Research & Development', 1),
(42, 'Restaurants & Food/Beverage Services', 1),
(43, 'Lodging', 1),
(44, 'Travel & Tourism', 1),
(45, 'Recreation, Amusements & Attractions', 1),
(46, 'Early Childhood Development & Services', 1),
(47, 'Counseling & Mental Health Services', 1),
(48, 'Family & Community Services', 1),
(49, 'Personal Care Services', 1),
(50, 'Consumer Services', 1),
(51, 'Network Systems', 1),
(52, 'Information Support & Services', 1),
(53, 'Web & Digital Communications', 1),
(54, 'Programming & Software Development', 1),
(55, 'Correction Services', 1),
(56, 'Emergency & Fire Management Services', 1),
(57, 'Security & Protective Services', 1),
(58, 'Law Enforcement Services', 1),
(59, 'Legal Services', 1),
(60, 'Production', 1),
(61, 'Manufacturing Production Process Development', 1),
(62, 'Maintenance, Installation & Repair', 1),
(63, 'Quality Assurance', 1),
(64, 'Logistics & Inventory Control', 1),
(65, 'Health, Safety & Environmental Assurance', 1),
(66, 'Marketing Management', 1),
(67, 'Professional Sales', 1),
(68, 'Merchandising', 1),
(69, 'Marketing Communications', 1),
(70, 'Marketing Research', 1),
(71, 'Engineering & Technology', 1),
(72, 'Science & Math', 1),
(73, 'Transportation Operations', 1),
(74, 'Logistics Planning & Management Services', 1),
(75, 'Warehousing & Distribution Center Operations', 1),
(76, 'Facility & Mobile Equipment Maintenance', 1),
(77, 'Transportation Systems/Infrastructure Planning, Management & Regulation', 1),
(78, 'Health, Safety & Environmental Management', 1),
(79, 'Sales & Service', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
