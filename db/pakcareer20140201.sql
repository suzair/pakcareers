-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 31, 2014 at 11:16 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pakcareers`
--

-- --------------------------------------------------------

--
-- Table structure for table `age`
--

CREATE TABLE IF NOT EXISTS `age` (
  `age_id` int(11) NOT NULL AUTO_INCREMENT,
  `age_title` varchar(255) NOT NULL,
  `age_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`age_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE IF NOT EXISTS `career` (
  `career_id` int(11) NOT NULL AUTO_INCREMENT,
  `career_title` varchar(255) NOT NULL,
  `occupation_id` int(11) NOT NULL,
  `career_worksummary` text NOT NULL,
  `career_relatedenvironment` varchar(255) NOT NULL,
  `career_duties` text NOT NULL,
  `career_context` text NOT NULL,
  `career_knowledgeRequired` text NOT NULL,
  `career_skillsRequired` text NOT NULL,
  `career_abilitiesrequired` text NOT NULL,
  `occupationgroup_id` int(11) NOT NULL,
  `career_photo` varchar(255) NOT NULL,
  `career_video` varchar(255) NOT NULL,
  `career_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`career_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `careertrack`
--

CREATE TABLE IF NOT EXISTS `careertrack` (
  `careertrack_id` int(11) NOT NULL AUTO_INCREMENT,
  `careertrack_title` varchar(255) NOT NULL,
  `careertrack_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`careertrack_id`,`careertrack_title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `career_careertrack`
--

CREATE TABLE IF NOT EXISTS `career_careertrack` (
  `ct_id` int(11) NOT NULL AUTO_INCREMENT,
  `career_id` int(11) NOT NULL,
  `careertrack_id` int(11) NOT NULL,
  PRIMARY KEY (`ct_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `career_fieldofwork`
--

CREATE TABLE IF NOT EXISTS `career_fieldofwork` (
  `cf_id` int(11) NOT NULL AUTO_INCREMENT,
  `career_id` int(11) NOT NULL,
  `fieldofwork_id` int(11) NOT NULL,
  PRIMARY KEY (`cf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `career_occupations`
--

CREATE TABLE IF NOT EXISTS `career_occupations` (
  `career_occupation_id` int(11) NOT NULL AUTO_INCREMENT,
  `career_id` int(11) NOT NULL,
  `occupation_id` int(11) NOT NULL,
  PRIMARY KEY (`career_occupation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `career_studytrack`
--

CREATE TABLE IF NOT EXISTS `career_studytrack` (
  `cs_id` int(11) NOT NULL AUTO_INCREMENT,
  `career_id` int(11) NOT NULL,
  `studytrack_id` int(11) NOT NULL,
  PRIMARY KEY (`cs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(255) NOT NULL,
  `city_status` tinyint(4) NOT NULL DEFAULT '1',
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(255) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `country_logo` varchar(255) NOT NULL,
  `country_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(255) NOT NULL,
  `course_levelofeducation` varchar(255) NOT NULL,
  `course_degreeprogram` varchar(255) NOT NULL,
  `course_duration` varchar(255) NOT NULL,
  `course_institution` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `course_fieldofeducation` varchar(255) NOT NULL,
  `studytrack_id` int(11) NOT NULL,
  `course_fee` varchar(255) NOT NULL,
  `course_admissiondeadline` datetime NOT NULL,
  `course_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE IF NOT EXISTS `education` (
  `education_id` int(11) NOT NULL AUTO_INCREMENT,
  `education_title` varchar(255) NOT NULL,
  `education_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`education_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE IF NOT EXISTS `experience` (
  `experience_id` int(11) NOT NULL AUTO_INCREMENT,
  `experience_title` varchar(255) NOT NULL,
  `experience_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`experience_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fieldofwork`
--

CREATE TABLE IF NOT EXISTS `fieldofwork` (
  `fieldofwork_id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldofwork_title` varchar(255) NOT NULL,
  `fieldofwork_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`fieldofwork_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobgroup`
--

CREATE TABLE IF NOT EXISTS `jobgroup` (
  `jobgroup_id` int(11) NOT NULL AUTO_INCREMENT,
  `jobgroup_title` varchar(255) NOT NULL,
  `jobgroup_status` tinyint(4) NOT NULL DEFAULT '1',
  `occupation_id` int(11) NOT NULL,
  PRIMARY KEY (`jobgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_title` varchar(255) NOT NULL,
  `jobsector_id` int(11) NOT NULL,
  `positions` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `agegroup_id` int(11) NOT NULL,
  `education_id` int(11) NOT NULL,
  `qualification` int(11) NOT NULL,
  `experience_id` int(11) NOT NULL,
  `salaryrange_id` int(11) NOT NULL,
  `jobtype_id` int(11) NOT NULL,
  `posted_date` datetime NOT NULL,
  `last_date` datetime NOT NULL,
  `organization_id` int(11) NOT NULL,
  `where_to_apply` varchar(255) NOT NULL,
  `jobsource_id` int(11) NOT NULL,
  `job_image` varchar(255) NOT NULL,
  `occupation_id` int(11) NOT NULL,
  `jobgroup_id` int(11) NOT NULL,
  `job_view` int(11) NOT NULL,
  `jobs_status` tinyint(4) NOT NULL DEFAULT '1',
  `occupationgroup_id` int(11) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobsector`
--

CREATE TABLE IF NOT EXISTS `jobsector` (
  `jobsector_id` int(11) NOT NULL AUTO_INCREMENT,
  `jobsector_title` varchar(255) NOT NULL,
  `jobsector_logo` varchar(255) NOT NULL,
  `jobsector_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`jobsector_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobsource`
--

CREATE TABLE IF NOT EXISTS `jobsource` (
  `jobsource_id` int(11) NOT NULL AUTO_INCREMENT,
  `jobsource_title` varchar(255) NOT NULL,
  `jobsource_logo` varchar(255) NOT NULL,
  `jobsource_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`jobsource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobs_careertrack`
--

CREATE TABLE IF NOT EXISTS `jobs_careertrack` (
  `jc_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `careertrack_id` int(11) NOT NULL,
  PRIMARY KEY (`jc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobs_fieldofwork`
--

CREATE TABLE IF NOT EXISTS `jobs_fieldofwork` (
  `jf_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `fieldofwork_id` int(11) NOT NULL,
  PRIMARY KEY (`jf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jobtype`
--

CREATE TABLE IF NOT EXISTS `jobtype` (
  `jobtype_id` int(11) NOT NULL AUTO_INCREMENT,
  `jobtype_title` varchar(255) NOT NULL,
  `jobtype_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`jobtype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `management`
--

CREATE TABLE IF NOT EXISTS `management` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rank_id` int(11) NOT NULL,
  `last_login` datetime NOT NULL,
  `last_ip` varchar(255) NOT NULL,
  `user_status` int(11) NOT NULL,
  `useremail` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `occupation`
--

CREATE TABLE IF NOT EXISTS `occupation` (
  `occupation_id` int(11) NOT NULL AUTO_INCREMENT,
  `occupation_title` varchar(255) NOT NULL,
  `occupation_othertitles` text NOT NULL,
  `occupationgroup_id` int(11) NOT NULL,
  `fieldofwork_id` int(11) NOT NULL,
  `careertrack_id` int(11) NOT NULL,
  `occupation_status` tinyint(4) NOT NULL DEFAULT '1',
  `occupation_video` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`occupation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `occupationgroup`
--

CREATE TABLE IF NOT EXISTS `occupationgroup` (
  `occupationgroup_id` int(11) NOT NULL AUTO_INCREMENT,
  `occupationgroup_title` varchar(255) NOT NULL,
  `occupationgroup_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`occupationgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE IF NOT EXISTS `organization` (
  `organization_id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_name` varchar(255) NOT NULL,
  `organization_logo` varchar(255) NOT NULL,
  `organization_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `salaryrange`
--

CREATE TABLE IF NOT EXISTS `salaryrange` (
  `salaryrange_id` int(11) NOT NULL AUTO_INCREMENT,
  `salaryrange_title` varchar(255) NOT NULL,
  `salaryrange_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`salaryrange_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `studytrack`
--

CREATE TABLE IF NOT EXISTS `studytrack` (
  `studytrack_id` int(11) NOT NULL AUTO_INCREMENT,
  `studytrack_title` varchar(255) NOT NULL,
  `studytrack_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`studytrack_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
