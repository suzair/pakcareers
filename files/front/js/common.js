// JavaScript Document
$(document).ready(function(e) {
    
	// job section
	
	$(document).on("click", ".js-submit-btn", function(e){
		e.preventDefault();
		showAjaxLoader();
		var form  = $(this).closest(".js-search-form");
		form.submit();
	});
});


function showAjaxLoader(){
	var shadow = $(".shadow");
	var loader = $(".loader");
	shadow.css({
		opacity: 0.5
	});
	shadow.fadeIn('fast',function(){
		loader.show();
	});
}

// job section

