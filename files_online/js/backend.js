$(document).ready(function(e) {
 
	var tableWithName = ["country" ,"city", "organization"]
	$(".search_field").keydown(function(){
		var tableName  = $(this).attr("name");
		var val = $(this).val()
		var field = tableWithName.indexOf(tableName) === -1 ? tableName+ "_title" : tableName+ "_name";
		search_records(tableName,field, val)
	})
	$(".search-course-field").keydown(function(){
		
		var tableName  = $(this).attr("name");
		var val = $(this).val()
		var field = $(this).attr('id');//tableWithName.indexOf(tableName) === -1 ? tableName+ "_title" : tableName+ "_name";
		search_records(tableName,field, val)
	});
	
	$(".search_user_field").keydown(function(e) {
        var tableName  = "management";
		var val = $(this).val()
		var field = "name";//tableWithName.indexOf(tableName) === -1 ? tableName+ "_title" : tableName+ "_name";
		search_records(tableName,field, val)
    });
	
	$(".search-career-field").keydown(function(){
		
		var tableName  = $(this).attr("name");
		var val = $(this).val()
		var field = $(this).attr('id');//tableWithName.indexOf(tableName) === -1 ? tableName+ "_title" : tableName+ "_name";
		search_records(tableName,field, val)
	});
	$(".search-job-field").keydown(function(){
		
		var tableName  = $(this).attr("name");
		var val = $(this).val()
		var field = $(this).attr('id');//tableWithName.indexOf(tableName) === -1 ? tableName+ "_title" : tableName+ "_name";
		search_records(tableName,field, val)
	});
	
});



function search_records(table, field, search_string){
	data= {
			table : table,
			field : field,
			search_string : search_string
		}
	$.ajax({
		url: baseURL+'welcome/search',
		data : data,
		type : 'GET',
		success: function(data){
			$(".data-section").html(data)
		}
		
	});	
}


function drawdonut(container ,title,chartTitle, chartData,dataLabel){
	//chartData =[["Females encouraged to apply (Males can also apply)",50],["Gender Does not matter",50]];// "["+chartData+"]";
	console.log(chartData);
	$(container).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: title
        },
        tooltip: {
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: dataLabel,
                    color: '#000000',
                    connectorColor: '#000000',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            type: 'pie',
            name: chartTitle,
            data: chartData 
        }]
		
    });
	//console.log(data)
}